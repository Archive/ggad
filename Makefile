
XMLFILES= \
        book.xml    \
        canvas.xml    \
        canvasitem.xml    \
        checklist.xml    \
        customwidget.xml    \
        dialogs.xml    \
        faqs.xml    \
        gdk.xml    \
        glib.xml    \
        gtk.xml    \
        headers.xml    \
        hierarchy.xml    \
	intro.xml	\
	listing.xml	\
        main.xml    \
        objects.xml    \
        online.xml    \
        sourcetree.xml    \
        startup.xml

##        xml.xml
##        corba.xml    \
##        guile.xml    \
##        gmodule.xml    \

OTHERFILES=Makefile

CODEFILES=code/hello/hello.c code/quotes/gtkbutton.h code/quotes/gtkbutton.c gnome-hello/src/hello.c gnome-hello/src/app.c gnome-hello/src/app.h gnome-hello/src/menus.h gnome-hello/src/menus.c  code/gtkev/gtkev.h code/gtkev/gtkev.c code/canvas-example/canvas-example.c

ENCODEFILES=gnome-hello/doc/C/Makefile.am

SNIPPETS = 	\
snippets/hello.enc  		\
snippets/hello-widgets.enc	\
snippets/hello-signals.enc 	\
snippets/hello-main.enc		\
snippets/hello-quit.enc		\
snippets/button-typemacros.enc	\
snippets/button-structs.enc	\
snippets/button-functions.enc	\
snippets/button-signal-enum.enc	\
snippets/button-classinit-decls.enc	\
snippets/button-emit-pressed.enc	\
snippets/button-get-type.enc	\
snippets/button-class-init.enc	\
snippets/button-init.enc	\
snippets/button-new.enc		\
snippets/button-signals-array.enc	\
snippets/gnomehello.enc		\
snippets/gnomehello-popttable.enc \
snippets/gnomehello-parsing.enc \
snippets/gnomehello-app.enc	\
snippets/gnomehello-apph.enc	\
snippets/gnomehello-menus.enc	\
snippets/gnomehello-menush.enc	\
snippets/gtkevh.enc		\
snippets/gtkev.enc		\
snippets/gtkevstructs.enc	\
snippets/gtkevdestroy.enc	\
snippets/gtkevrealize.enc	\
snippets/gtkevunrealize.enc	\
snippets/gtkevsizerequest.enc	\
snippets/gtkevsizeallocate.enc	\
snippets/gtkevdraw.enc		\
snippets/gtkevexpose.enc	\
snippets/gtkevpaint.enc		\
snippets/gtkevpaintevent.enc	\
snippets/gtkevdrawfocus.enc	\
snippets/gtkevevent.enc		\
snippets/gtkevinit.enc


ENCODED =	\
gnome-hello/doc/C/Makefile.am.enc \
gnome-hello/src/Makefile.am.enc

all: exemel html text

sgml: book.sgml

book.sgml: $(XMLFILES) $(SNIPPETS) $(ENCODED) ./exemel/src/exemel Makefile
	./exemel/src/exemel -d book.xml


html: book.html

book.html: $(XMLFILES) $(SNIPPETS) $(ENCODED) ./exemel/src/exemel Makefile
	./exemel/src/exemel -h book.xml

text: book.txt

book.txt: $(XMLFILES) $(SNIPPETS) $(ENCODED) ./exemel/src/exemel Makefile
	./exemel/src/exemel -t book.xml

book.zip: book.txt book.html
	zip -r book.zip book.txt book.html figures/*.png

book.tar.gz: book.txt book.html book.xml tags.txt Omitted.txt
	(cd .. && tar cvzf book2/book.tar.gz book2/book.txt book2/book.html book2/*.xml book2/figures/*.png book2/snippets/*.enc book2/tags.txt book2/Omitted.txt)

GGAD.tar.gz: book.html
	(cd .. && tar cvzf GGAD/GGAD.tar.gz GGAD/book.html GGAD/figures/*.png)

$(SNIPPETS): $(CODEFILES) code/extract-code
	code/extract-code $(CODEFILES)

$(ENCODED): $(ENCODEFILES)

%.enc : %
	./exemel/src/exemel -e -o $@ $<

clean:
	/bin/rm -rf *.txt *.aux *.dvi *.ps *.html *.rtf *.gz *~ *.log *.toc *.zip *.tar snippets/* `find -name "*~"` `find -name "*.o"`  code/hello/hello

exemel:
	(cd ./exemel && make)



