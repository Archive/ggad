

The short answer is, you need to ignore everything but the "DocBook"
subdirectory. Go into DocBook, and run db2html, db2ps, or whatever you
like. You will need to set up the GNOME PNG variant of DocBook, which
is mentioned below and may also be described by the GNOME
Documentation Project (www.gnome.org/gdp).

Josh Buhl contributed the following instructions:

Contents:

I.   Getting it
II.  Building the html, txt, and fot versions w/o sgmltools
III. Building the dvi, ps, html, or txt versions using sgmltools


I. Getting it
   
   First, you need to download the GGAD source module from the CVS
    server. You do this by executing at the prompt in order (at the
    cvs password just hit enter)

    $ export CVSROOT=':pserver:anonymous@anoncvs.gnome.org:/cvs/gnome'
    $ cvs login
    (Logging in to anonymous@anoncvs.gnome.org)
    CVS password: <enter>
    $ cvs -z3 checkout GGAD
    
    This downloads and unpacks the module into the directiory GGAD.

    You'll need the CVS modules popt and gnome-xml installed on your
    system if you want to build the html or txt versions w/o using sgmltools.

    If you want to build a printable version (i.e. postscript) or if
    you don't want to make and install the popt and gnome-xml modules,
    you'll need to have sgmltools version 2 or higher installed. On
    Debian systems do an

    # apt-get install sgmltools-2

    as root or select and install the package using dselect. On other
    systems install the corresponding package appropriately.

    You also need the file docbook-gnome-3.1.dtd which is in the
    gnome-docu CVS module.  

    $ cvs -z3 checkout gnome-docu/debian/docbook-gnome/docbk31-gnome/docbook-gnome-3.1.dtd

    The dtd file needs to be installed
    in the proper directory. I'm running debian potato and copied it
    to
    
    /usr/lib/sgml/dtd/docbook-gnome-3.1.dtd

    You then have to add a corresponding entry to the sgml.catalog
    file. On Debian potato this is in /etc/sgml.catalog and I
    added the line 

    PUBLIC "-//GNOME//DTD DocBook PNG Variant V1.0//EN" "dtd/docbook-gnome-3.1.dtd"



II.  Building the html, txt, and fot versions w/o sgmltools
     
     Enter the code subdirectory, edit the 'extract-code' script and 
     change the $root variable to the correct value (i.e. the location
     of the GGAD directory) on your system.

     In the exemel subdirectory type at the prompt in order

     $ aclocal
     $ autoheader
     $ autoconf
     $ automake -a
     $ ./configure

     
     Type make in the exemel subdirectory to build the exemel tool. If
     you get the message

    exemel.cc: In function `int main(int, char **)':
    exemel.cc:117: initialization to `char **' from `const char **'
    discards qualifiers 

    
    or similar, then edit the file src/exemel.cc and add the keyword
    const to the beginning of lines 117 and 120, like this:

	 const char** cfiles = poptGetArgs(pctx);

	 const char** tmp = cfiles;

    and then type make again in the exemel directory.


    Once the exemel tool is built, type make int the toplevel
    (i.e. GGAD) directory.  This should generate the files book.html,
    book.txt, and book.fot.


III. Building the dvi, ps, html, or txt versions using sgmltools

     To get all the pretty pictures in the book for the postscript
     version, you need encapsulated postscript versions of all the
     figures with the correct physical dimensions.  I don't know of a
     filter which converts png to eps, so if the eps versions of the
     figures aren't in the GGAD module, feel free to contact me and
     I'll send you the ones I made (email me at
     josh@math.uni-bonn.de).  Make a subdirectory called figures in
     the DocBook directory and copy the figures *.eps to this
     directory. (BTW, if you know of a filter, please let me know
     about it.)

     After installing the docbook-gnome-3.1.dtd in the correct
     directory and adding the entry in /etc/sgml.catalog (see above)
     enter the GGAD/DocBook subdirectory and type

     $ sgmltools -b ps book.sgml
     
     And hope it works.  Most likely, though, jadetex will exit with an
     error like this 
     
     ! TeX capacity exceeded, sorry [save size=5000].

     In this case you'll have to change the maximum save size for
     jadetex and TeX. On my Debian potato system the config file is

	 /etc/texmf/texmf.cnf

     and I changed the lines

	save_size.context = 5000

      and

	save_size.jadetex = 5000 

     to

	save_size.context = 10000 %increased from 5000 -jb
	save_size.jadetex =10000 %increased from 5000 -jb

	
     Now try `sgmltools -b ps book.sgml' again.  This should generate
     book.ps To build the html, dvi, or txt versions, just replace ps
     with html, dvi, or txt. The html version appears in a `book'
     subdirectory.


     There are also  book-ps.dsl and  book-html.dsl stylesheets
     included in the GGAD/DocBook directory.  If you use the
book-ps.dsl, the
     book will come out about 180 pages shorter!  To try it out you
     first need to edit the path in the second line from the top to
     match your system. For my Debian system I had to change

<!ENTITY dbstyle SYSTEM "/usr/lib/sgml/stylesheets/nwalsh-modular/print/docbook.dsl" CDATA DSSSL>

to 

<!ENTITY dbstyle SYSTEM "/usr/lib/sgml/stylesheet/dsssl/docbook/nwalsh/print/docbook.dsl " CDATA DSSSL>
    

     Now type

     $ sgmltools -b ps -s book-ps.dsl book.sgml

     If the text is too small (180 pages have to go somewhere!) try
     editing some of the parameters in this stylesheet and build the
     book again.

