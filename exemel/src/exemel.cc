// -*- C++ -*-
// Copyright 1999 Havoc Pennington. All Rights Reserved.

extern "C" {
#include "popt.h"
#include <stdio.h>
#include <errno.h>
}

#include <string>
#include <vector>

#include "output.h"
#include "text.h"
#include "html.h"
#include "docbook.h"
#include "tags.h"
#include "node.h"
#include "util.h"

static int do_the_spew = false;
static int text_mode = false;
static int html_mode = false;
static int docbook_mode = false;
static int encode_mode = false;
static int tableize = false; // convert function/macro lists to tables
static char* outfilename = 0;

struct poptOption options[] = {
  { 
    "text",
    't',
    POPT_ARG_NONE,
    &text_mode,
    0
  },
  { 
    "html",
    'h',
    POPT_ARG_NONE,
    &html_mode,
    0
  },
    { 
    "docbook",
    'd',
    POPT_ARG_NONE,
    &docbook_mode,
    0
  },
  {
    "encode",
    'e',
    POPT_ARG_NONE,
    &encode_mode,
    0
  },
  { 
    "outfile",
    'o',
    POPT_ARG_STRING,
    &outfilename,
    0
  },
  { 
    "spew",
    's',
    POPT_ARG_NONE,
    &do_the_spew,
    0
  },
  {
    "tableize",
    '\0',
    POPT_ARG_NONE,
    &tableize,
    0
  },
  {
    NULL,
    '\0',
    0,
    NULL,
    0
  }
};

static void
output_node(Node* node,
            Output* out,
            FILE* f)
{
  // This is clearly a pointless function now, but once it wasn't.
  out->output(f, node);
  if (do_the_spew)
    node->spew(NULL, 0);
}

int main (int argc, char** argv)
{
  poptContext pctx = poptGetContext("exemel", 
                                    argc, 
                                    argv,
                                    options,
                                    0);

  int rc = poptGetNextOpt(pctx);

  if (rc < -1)
    {
      fprintf(stderr, "%s: %s\n",
              poptBadOption(pctx, POPT_BADOPTION_NOALIAS),
              poptStrerror(rc));
      exit(1);
    }

  char** cfiles = poptGetArgs(pctx);

  vector<string> files;
  char** tmp = cfiles;
  while (tmp && *tmp)
    {
      files.push_back(*tmp);
      ++tmp;
    }

  poptFreeContext(pctx);
  pctx = 0;

  if (files.empty())
    {
      fprintf(stderr, "No files specified.\n");
      exit(1);
    }
  else 
    {
      printf("Input files: ");
      vector<string>::const_iterator i = files.begin();
      while (i != files.end())
        {
          printf(" %s", (*i).c_str());
          ++i;
        }
      printf("\n");
    }

  

  Output* out = 0;

  string suffix;

  if (text_mode)
    {
      printf("Using text mode.\n");
      out = new Text;
      suffix = ".txt";
    }
  else if (html_mode)
    {
      printf("Using html mode.\n");
      out = new HTML;
      suffix = ".html";
    }
  else if (docbook_mode)
    {
      printf("Using DocBook mode.\n");
      out = new DocBook;
      suffix = ".sgml";
    }
  else if (encode_mode)
    {
      printf("Using encode mode.\n");
      
    }
  else 
    {
      fprintf(stderr, "No output mode given.\n");
      exit(1);
    }

  FILE* outfile = 0;
  
  string realoutfile;

  if (outfilename == 0)
    {      
      realoutfile = "book" + suffix;
      printf("No output filename; sending to %s\n", realoutfile.c_str());
    }
  else
    {
      realoutfile = outfilename;
    }

  outfile = fopen(realoutfile.c_str(), "w");
  
  if (outfile == 0)
    {
      fprintf(stderr, "Couldn't open output file %s: %s\n",
              outfilename, strerror(errno));
      exit(1);
    }

  if (out != 0)
    {
      vector<string>::const_iterator i = files.begin();
      while (i != files.end())
        {
          printf("Outputting %s\n", (*i).c_str());
          
          xmlDocPtr doc = xmlParseFile((*i).c_str());

          if (doc != 0 && doc->root != 0)
            {          
              xmlNodePtr booknode = doc->root;

              /* skip over anything that doesn't look like a <book> tag */
              while (booknode && booknode->name &&
                     (xmlStrcmp(booknode->name, (const CHAR*)BOOK_T) != 0))
                booknode = booknode->next;

              if (booknode)
                {
                  const CHAR* t = booknode->name;
                  if (t == 0 || (xmlStrcmp(t, (const CHAR*)BOOK_T) != 0))
                    {
                      fprintf(stderr, "This does not look like a book file, root name is %s.\n", t);
                      exit(1);
                    }
                  else
                    {
                      //                  freopen("book.log", "w", stderr);
                      Node* n = new Node;
                      n->set(booknode);
                      Counter counter;
                      postprocess_tree(n, counter, tableize);
                      output_node(n, out, outfile);
                    }
                }
              else
                {
                  fprintf(stderr, "No <book> node found\n");
                  exit(1);
                }
            }
          else 
            {
              fprintf(stderr, "Failed to load or parse %s.\n", (*i).c_str());
              exit(1);
            }
          
          if (doc)
            xmlFreeDoc(doc);
          doc = 0;

          fflush(outfile);
          
          ++i;
        }
    }
  else if (encode_mode)
    {
      xmlDocPtr doc = xmlNewDoc((const CHAR*)"1.0");

      vector<string>::const_iterator i = files.begin();
      while (i != files.end())
        {
          printf("Outputting %s\n", (*i).c_str());
          
          char* data = file_to_string(*i);

          char* encoded = (char*)xmlEncodeEntities(doc, (CHAR*)data);

          fputs(encoded, outfile);

          free(data);
          free(encoded);

          fflush(outfile);
          
          ++i;
        }

      if (doc) xmlFreeDoc(doc);
      doc = 0;
    }
  else 
    {
      fprintf(stderr, "WARNING: Internal error, don't know what to do.\n");
      exit(1);
    }

  if (outfile) 
    fclose(outfile);

  printf("\n");

  return 0;
}

