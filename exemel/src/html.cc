// -*- C++ -*-
// Copyright 1999 Havoc Pennington. All Rights Reserved.


#include "html.h"
#include "tags.h"
#include "util.h"
#include <map>

#include <time.h>

static void 
encode_text_for_html(string & content)
{
  // Encode any < and > in the content

  // We have a lame hack in here since the same content can get
  // "encoded" more than once since get_content() is recursive.

  unsigned int j = 0;
  while (j < content.size())
    {
      switch (content[j]) {
      case '<':
        content.replace(j, 1, "&lt;");
        break;
      case '>':
        content.replace(j, 1, "&gt;");
        break;
      case '&':
        // Note that an & we inserted ourselves *this time* is skipped
        // over since it replaces the <, >, or whatever

        // But we have to check that the & doesn't begin a &amp; from a 
        //  previous call.

        if (content.size() - j > 3) 
          {
            if (content.size() - j > 4) // at least four characters left ("amp;")
              {
                if (content[j+1] == 'a' &&
                    content[j+2] == 'm' && 
                    content[j+3] == 'p' &&
                    content[j+4] == ';')
                  {
                    ++j;
                    continue;
                  }
              }
            if (content[j+1] == 'l' &&
                content[j+2] == 't' && 
                content[j+3] == ';')
              {
                ++j;
                continue;
              }
            if (content[j+1] == 'g' &&
                content[j+2] == 't' && 
                content[j+3] == ';')
              {
                ++j;
                continue;
              }
          }

        content.replace(j, 1, "&amp;");
      default:
        break;
      }

      ++j;
    }
}

static void 
html_fputs(const char* s, FILE* f)
{
#if 0
  string enc = s;

  encode_text_for_html(enc);
#endif

  fputs(s, f);
}

static void
encode_monospace(string & s)
{
  s = "<tt>" + s + "</tt>";
}

static void
encode_italics(string & s)
{
  s = "<i>" + s + "</i>";
}

static string get_content(Node* n);

// Get plain text body content until some other kind of node
// is encountered; increment i accordingly
static string 
get_content_until(vector<Node*>::const_iterator & i, 
                  vector<Node*>::const_iterator end)
{
  string content;

  while (i != end)
    {
      switch ((*i)->type())
        {
        case Node::TEXT:
          {
            string tmp = (*i)->text();
            encode_text_for_html(tmp);
            content += tmp;
          }
          break;

        case Node::LABEL:
          {
            // nothing needed
          }
          break;

        case Node::CHAPREF:
          {
            content += "Chapter ";
            const Counter* c = get_ref(*i);
            content += c ? c->chapter_str() : string("[FIXME: Chapter number]");
            if (c == 0)
              fprintf(stderr, "WARNING: reference `%s' is undefined.\n",
                      get_content(*i).c_str());
          }
          break;

        case Node::SECREF:
          {
            content += "Section ";
            const Counter* c = get_ref(*i);
            content += c ? c->section_str() : string("[FIXME: Section number]");
            if (c == 0)
              fprintf(stderr, "WARNING: reference `%s' is undefined.\n",
                      get_content(*i).c_str());
          }
          break;
            
        case Node::FIGREF:
          {
            content += "Figure ";
            const Counter* c = get_ref(*i);
            content += c ? c->figure_str() : string("[FIXME: Figure number]");
            if (c == 0)
              fprintf(stderr, "WARNING: reference `%s' is undefined.\n",
                      get_content(*i).c_str());
          }
          break;

        case Node::TABREF:
          {
            content += "Table ";
            const Counter* c = get_ref(*i);
            content += c ? c->table_str() : string("[FIXME: Table number]");
            if (c == 0)
              fprintf(stderr, "WARNING: reference `%s' is undefined.\n",
                      get_content(*i).c_str());
          }
          break;

        case Node::SIDEREF:
          {
            content += "Sidebar ";
            content += "XXX";
          }
          break;

        case Node::MLREF:
          {
            content += "Macro Listing ";
            const Counter* c = get_ref(*i);
            content += c ? c->maclist_str() : string("[FIXME: Macro Listing number]");
            if (c == 0)
              fprintf(stderr, "WARNING: reference `%s' is undefined.\n",
                      get_content(*i).c_str());
          }
          break;

        case Node::FLREF:
          {
            content += "Function Listing ";
            const Counter* c = get_ref(*i);
            content += c ? c->funclist_str() : string("[FIXME: Function Listing number]");
            if (c == 0)
              fprintf(stderr, "WARNING: reference `%s' is undefined.\n",
                      get_content(*i).c_str());
          }
          break;

        case Node::WIDGET:
        case Node::C:
        case Node::FILE:
        case Node::FUNCTION:
        case Node::HEADER:
        case Node::MACRO:
        case Node::MFOUR:
        case Node::MODULE:
        case Node::TT:
          {
            string tmp = get_content(*i);

            strip_newlines(tmp);

            encode_text_for_html(tmp);

            encode_monospace(tmp);

            content += tmp;
          }
          break;

        case Node::URL:
          {
            string tmp = get_content(*i);

            strip_newlines(tmp);

            content += "<a href=\"";
            content += tmp;
            content += "\">";
            content += tmp;
            content += "</a>";
          }
          break;          

        case Node::SIGNAL:
          {
            string tmp = get_content(*i);
            
            strip_newlines(tmp);

            tmp = "\"" + tmp + "\"";

            encode_monospace(tmp);
            
            content += tmp;
          }
          break;

        case Node::TERM:
        case Node::EM:
          {
            string tmp = get_content(*i);

            strip_newlines(tmp);

            encode_text_for_html(tmp);

            encode_italics(tmp);

            content += tmp;
          }
          break;
          
        case Node::CXX:
          content += "C++";
          break;

        case Node::GLIB:
          content += "glib";
          break;

        case Node::GDK:
          content += "Gdk";
          break;

        case Node::GTK:
          content += "Gtk+";
          break;

        case Node::GNOME:
          content += "Gnome";
          break;

        case Node::IMLIB:
          content += "Imlib";
          break;

        case Node::LINUX:
          content += "Linux"; 
          break;

        case Node::ORBIT:
          content += "ORBit";
          break;

        case Node::UNIX:
          content += "UNIX";
          break;

        default:
          goto done;
          break;
        };

      ++i;
    }

 done:

  return content;
}

// Consolidate all child nodes; only non-block nodes allowed
static string
get_content(Node* n)
{
  string content;

  const vector<Node*> & children = n->children();
  
  vector<Node*>::const_iterator i = children.begin();

  content = get_content_until(i, children.end());

  if (i != children.end())
    {
      fprintf(stderr, "WARNING: Encountered non-content block nodes in unexpected location.\n");

      while (i != children.end())
        {
          fprintf(stderr, "WARNING:   Unexpected Node <%s> id %u\n",
                  (*i)->name().c_str(), (*i)->id());
          ++i;
        }
    }
  
  return content;
}

static void
output_frontmatter(FILE* f, Node* n)
{
  switch (n->type())
    {
    case Node::TITLE:
      {
        string title;
        title = get_content(n);

        strip_newlines(title);

        newline(f);
        html_fputs("<h1>", f);
        html_fputs(title.c_str(), f);
        html_fputs("</h1>", f);
        newline(f);
        return;
      }
      break;
      
    case Node::AUTHOR:
      {
        string author;
        author = get_content(n);

        strip_newlines(author);

        newline(f);
        html_fputs("<h2>", f);
        html_fputs(author.c_str(), f);
        html_fputs("</h2>", f);
        newline(f);
        return;
      }
      break;

    case Node::COPYRIGHT:
      {
        string copyright;
        copyright = get_content(n);

        strip_newlines(copyright);

        newline(f);
        html_fputs("<h2>", f);
        html_fputs(copyright.c_str(), f);
        html_fputs("</h2>", f);
        newline(f);
      }
      break;

    default:
      fprintf(stderr, "WARNING: Internal error, node <%s> is not front matter\n", n->name().c_str());
      break;
    }
}

static void 
output_code(FILE* f, Node* n, bool toplevel)
{
  if (toplevel) newline(f);
  html_fputs("\n<pre>\n", f);

  string content = get_content(n);

  html_fputs(content.c_str(), f);

  html_fputs("\n</pre>\n", f);
  newline(f);  
  if (toplevel) newline(f);
}

static void
output_list(FILE* f, Node* n)
{
  newline(f);
  //  newline(f);

  if (n->type() == Node::BL)
    {
      html_fputs("<ul>\n", f);
    }
  else if (n->type() == Node::NL)
    {
      html_fputs("<ol>\n", f);
    }

  
  const vector<Node*> & children = n->children();
  
  vector<Node*>::const_iterator i = children.begin();
  vector<Node*>::const_iterator end = children.end();

  while (i != end)
    {
      if (n->type() == Node::BL && (*i)->type() != Node::LB)
        {
          fprintf(stderr, "WARNING: non-bullet immediately inside bullet list\n");
          ++i;
          continue;
        }
      else if (n->type() == Node::NL && (*i)->type() != Node::LN)
        {
          fprintf(stderr, "WARNING: non-number immediately inside number list\n");
          ++i;
          continue;
        }
      
      string content = get_content(*i);

      strip_newlines(content);

      html_fputs("<li> ", f);

      html_fputs(content.c_str(), f);

      newline(f);
      //     newline(f);
      
      ++i;
    }

  if (n->type() == Node::BL)
    {
      html_fputs("\n</ul>\n", f);
    }
  else if (n->type() == Node::NL)
    {
      html_fputs("\n</ol>\n", f);
    }

  newline(f);
}


static void
get_cells(Node* n, vector<string> & cells)
{
  const vector<Node*> & children = n->children();
  
  vector<Node*>::const_iterator i = children.begin();
  vector<Node*>::const_iterator end = children.end();

  while (i != end)
    {
      string content = get_content(*i);

      cells.push_back(content);

      ++i;
    }
}

static void
output_table(FILE* f, Node* n)
{
  const vector<Node*> & children = n->children();
  
  vector<Node*>::const_iterator i = children.begin();
  vector<Node*>::const_iterator end = children.end();
  
  string caption;

  vector<string> headers;

  vector<vector<string> > rows;

  while (i != end)
    {
      switch ((*i)->type())
        {
        case Node::CAPTION:
          caption = get_content(*i);
          break;
        case Node::TABLEHEAD:
          {
            get_cells(*i, headers);
          }
          break;
        case Node::TABLEBODY:
          {
            vector<string> row;
            get_cells(*i, row);
            rows.push_back(row);
          }
          break;
        case Node::LABEL:
          break;
        default:
          fprintf(stderr, "WARNING: Node <%s> id %u inside <table>!\n",
                  (*i)->name().c_str(), (*i)->id());
          break;
        }

      ++i;
    }

  string table = "<table border=\"1\">";
  table += "<caption><b>Table " + n->counter().table_str() + ":</b> " + 
    caption + "</caption>\n";

  table += "<tr>";

  vector<string>::iterator h = headers.begin();
  while (h != headers.end())
    {
      table += "<th>";
      table += *h;      
      ++h;
    }

  table += "\n";

  vector<vector<string> >::iterator vi = rows.begin();
  while (vi != rows.end())
    {
      table += "<tr>";
      vector<string>::iterator ri = (*vi).begin();
      while (ri != (*vi).end())
        {
          table += "<td>";
          table += *ri;
          ++ri;
        }

      table += "\n";

      ++vi;
    }

  table += "</table>\n";

  html_fputs(table.c_str(), f);
}


static void
output_funcsyn(FILE* f, Node* n)
{
  string content = funcsyn_to_func(n);
  
  strip_leading_trailing(content);

  encode_monospace(content);

  html_fputs(content.c_str(), f);

#if 0
  const vector<Node*> & children = n->children();
  
  vector<Node*>::const_iterator i = children.begin();
  vector<Node*>::const_iterator end = children.end();
  
  while (i != end)
    {
      string content;
      Node* c = *i;
              
      switch (c->type())
        {
        case Node::FUNCNAME:
          {
            content = get_content(c);
            
            strip_newlines(content);
            strip_leading_trailing(content);

            bool is_broken = false;
            unsigned int x = 0;
            while (x < content.size())
              {
                if (content[x] == '(')
                  {
                    is_broken = true;
                  }

                ++x;
              }
            
            if (is_broken)
              {
                string goodversion = xmlize_c_function(content);

                fprintf(stderr, "WARNING: <funcname> contains the entire function. The proper XML is:\n%s", goodversion.c_str());
              }

            content = "Name: " + content;
          }
          break;

        case Node::RETVAL:
          content = get_content(c);
          
          strip_newlines(content);
          strip_leading_trailing(content);

          content = "Returns: " + content;
          break;

        case Node::ARG:
          {
            content += "Argument: ";

            const vector<Node*> & ch = c->children();
            
            vector<Node*>::const_iterator j = ch.begin();
            vector<Node*>::const_iterator e = ch.end();
            
            while (j != e)
              {
                Node* q = *j;
            
                switch (q->type())
                  {
                  case Node::ARGTYPE:
                    {
                      string t = get_content(q);
                      strip_newlines(t);
                      strip_leading_trailing(t);
                      content += " " + t;
                    }
                    break;
                  case Node::ARGNAME:
                    {
                      string t = get_content(q);
                      strip_newlines(t);
                      strip_leading_trailing(t);
                      content += t;
                    }
                    break;
                    
                  default:
                    fprintf(stderr, "WARNING: <%s> inside an <arg>\n", 
                            q->name().c_str());                    
                    break;
                  }
                ++j;
              }
          }
          break;


        default:
          fprintf(stderr, "WARNING: <%s> inside a <funcsyn>\n", 
                  c->name().c_str());
          break;
        }


      if (!content.empty())
        {
          html_fputs(content.c_str(), f);
          newline(f);
        }

      ++i;
    }
#endif
}

static void
output_funclist(FILE* f, Node* n)
{
  newline(f);

  const vector<Node*> & children = n->children();
  
  vector<Node*>::const_iterator i = children.begin();
  vector<Node*>::const_iterator end = children.end();
  
  html_fputs("<p>\n", f);

  html_fputs("<table bgcolor=\"#EEEEFF\" width=\"100%\">", f);

  vector<string> rows;
  string caption;

  while (i != end)
    {
      if ((*i)->type() == Node::FUNCSYN)
        {
          string content = funcsyn_to_func(*i);
          
          strip_leading_trailing(content);

          content = "<tr><td> <font color=\"black\"><pre>" + content;
          content += "</pre></font> </td></tr>\n";

          rows.push_back(content);
        }
      else if ((*i)->type() == Node::FUNCHEADER)
        {
          if (!rows.empty())
            fprintf(stderr, "WARNING: <funcheader> should be before any <funcsyn>\n");

          string content = get_content(*i);

          strip_newlines(content);
          strip_leading_trailing(content);

          content = "<tr align=\"right\"><td> <font color=\"black\">&nbsp;<tt>#include &lt;" + content + "&gt;</tt> </font> </td></tr>\n";
          
          rows.push_back(content);
        }
      else if ((*i)->type() == Node::CAPTION)
        {
          string content = get_content(*i);

          strip_newlines(content);
          strip_leading_trailing(content);

          caption = "<caption><b>Function Listing " + (*i)->counter().funclist_str() + ":</b> " + content + "</caption>\n";
        }
      else if ((*i)->type() == Node::LABEL)
        {
          // Always ignored - processed earlier by the counter thing
        }
      else
        {
          fprintf(stderr, "WARNING: Node <%s> in <funclist>\n",
                  (*i)->name().c_str());
        }

      ++i;
    }

  html_fputs(caption.c_str(), f);

  vector<string>::iterator si = rows.begin();
  while (si != rows.end())
    {
      html_fputs((*si).c_str(), f);
      ++si;
    }

  html_fputs("</table>\n", f);

  html_fputs("</p>\n", f);

  newline(f);
}

static void
output_macsyn(FILE* f, Node* n)
{
  const vector<Node*> & children = n->children();
  
  vector<Node*>::const_iterator i = children.begin();
  vector<Node*>::const_iterator end = children.end();
  
  while (i != end)
    {
      string content;
      Node* c = *i;
            
      content = get_content(c);

      strip_leading_trailing(content);
  
      switch (c->type())
        {
        case Node::MACNAME:
          content = "Name: " + content;
          break;
          
        case Node::ARGNAME:
          content = "Arg: " + content;
          break;

        case Node::VOID:
          content = "(no args)";
          break;
          
        default:
          fprintf(stderr, "WARNING: <%s> inside a <macsyn>\n", 
                  c->name().c_str());
          break;
        }


      if (!content.empty())
        {
          html_fputs(content.c_str(), f);
          newline(f);
        }

      ++i;
    }
}

static void
output_maclist(FILE* f, Node* n)
{
  newline(f);

  const vector<Node*> & children = n->children();
  
  vector<Node*>::const_iterator i = children.begin();
  vector<Node*>::const_iterator end = children.end();
  
  html_fputs("<p>\n", f);

  html_fputs("<table bgcolor=\"#FFEEEE\" width=\"100%\">", f);

  vector<string> rows;
  string caption;

  while (i != end)
    {
      if ((*i)->type() == Node::MACSYN)
        {
          string content = macsyn_to_mac(*i);
          
          strip_leading_trailing(content);

          encode_monospace(content);

          content = "<tr><td> <font color=\"black\"><pre>" + content;
          content += "</pre></font> </td></tr>\n";

          rows.push_back(content);
        }
      else if ((*i)->type() == Node::MACHEADER)
        {
          if (!rows.empty())
            fprintf(stderr, "WARNING: <macheader> should be before any <macsyn>\n");

          string content = get_content(*i);

          strip_newlines(content);
          strip_leading_trailing(content);

          content = "<tr align=\"right\"><td> <font color=\"black\">&nbsp;<tt>#include &lt;" + content + "&gt;</tt> </font> </td></tr>\n";

          rows.push_back(content);
        }
      else if ((*i)->type() == Node::CAPTION)
        {
          string content = get_content(*i);

          strip_newlines(content);
          strip_leading_trailing(content);

          caption = "<caption><b>Macro Listing " + (*i)->counter().maclist_str() + ":</b> " + content + "</caption>\n";
        }
      else if ((*i)->type() == Node::LABEL)
        {
          // Always ignored - processed earlier by the counter thing
        }
      else
        {
          fprintf(stderr, "WARNING: Node <%s> in <maclist>\n",
                  (*i)->name().c_str());
        }

      ++i;
    }

  html_fputs(caption.c_str(), f);

  vector<string>::iterator si = rows.begin();
  while (si != rows.end())
    {
      html_fputs((*si).c_str(), f);
      ++si;
    }

  html_fputs("</table>\n", f);

  html_fputs("</p>\n", f);

  newline(f);
}


static void
output_figure(FILE* f, Node* n)
{
  const vector<Node*> & children = n->children();
  
  vector<Node*>::const_iterator i = children.begin();
  vector<Node*>::const_iterator end = children.end();

  string caption;
  string file;

  while (i != end)
    {
      switch ((*i)->type())
        {
        case Node::CAPTION:
          {
            string content = get_content(*i);
            caption = "Figure " + (*i)->counter().figure_str() + ": " + content;
          }
          break;
          
        case Node::IMAGEFILE:
          {
            string content = get_content(*i);
            strip_leading_trailing(content);
            file = content;
          }
          break;
          
        case Node::LABEL:
          break;

        default:
          fprintf(stderr, "WARNING: Node <%s> inside <figure>\n", (*i)->name().c_str());
          break;
          
        }

      ++i;
    }
  
  string all = "<p><center>";
  all += "<img src=\"";
  all += file;
  all += "\"></center></p>\n";
  all += "<p><center><b>";
  all += caption;
  all += "</b></center></p>\n";
  
  html_fputs(all.c_str(), f);
}

static void
output_paragraph(FILE* f, Node* n)
{
  const vector<Node*> & children = n->children();
  
  vector<Node*>::const_iterator i = children.begin();
  vector<Node*>::const_iterator end = children.end();

  while (i != end)
    {
      string content;
            
      content = get_content_until(i, end);

      strip_newlines(content);
      strip_leading_trailing(content);

      if (!content.empty())
        {
          html_fputs("<p> ", f);
          html_fputs(content.c_str(), f);
          newline(f);
          html_fputs("</p> ", f);
          newline(f);
        }

      if (i != end)
        {
          Node* c = *i;
          // non-content node, do the appropriate thing
          switch (c->type())
            {
            case Node::CODE:
              output_code(f, c, false);
              break;

            case Node::BL:
            case Node::NL:
              output_list(f, c);
              break;

            case Node::FUNCLIST:
              output_funclist(f, c);
              break;

            case Node::MACLIST:
              output_maclist(f, c);
              break;

            case Node::TABLE:
              output_table(f, c);
              break;

            case Node::COMMENT:
              {
                string content = get_content(n);
                fputs("<!-- ", f);
                fputs(content.c_str(), f);
                fputs("-->\n", f);
              }
              break;
              
            case Node::P:
            case Node::CAPTION:
            case Node::IMAGEFILE:
            case Node::TABLEHEAD:
            case Node::TABLEBODY:
            case Node::TABLECELL:
            case Node::MACHEADER:
            case Node::MACSYN:
            case Node::MACNAME:
            case Node::FUNCHEADER:
            case Node::FUNCSYN:
            case Node::FUNCNAME:
            case Node::RETVAL:
            case Node::ARG:
            case Node::ARGTYPE:
            case Node::ARGNAME:
            case Node::VOID:
            case Node::LB:
            case Node::LN:
            case Node::LABEL:
            case Node::CHAPREF:
            case Node::SECREF:
            case Node::FIGREF:
            case Node::TABREF:
            case Node::SIDEREF:
            case Node::MLREF:
            case Node::FLREF:
            case Node::INCLUDE:
            case Node::WIDGET:
            case Node::C:
            case Node::FILE:
            case Node::FUNCTION:
            case Node::HEADER:
            case Node::MACRO:
            case Node::MFOUR:
            case Node::MODULE:
            case Node::SIGNAL:
            case Node::TERM:
            case Node::EM:
            case Node::TT:
            case Node::URL:
            case Node::CXX:
            case Node::GLIB:
            case Node::GDK:
            case Node::GTK:
            case Node::GNOME:
            case Node::IMLIB:
            case Node::LINUX:
            case Node::ORBIT:
            case Node::UNIX:
            case Node::TEXT:
              fprintf(stderr, "WARNING: <%s> id %u should not be below <p>\n", c->name().c_str(), c->id());
              break;

            default:
              fprintf(stderr, "WARNING: Internal error, node <%s> id %u not handled in paragraph.\n",
                      c->name().c_str(), c->id());
              break;
            }

          ++i;
        }
    }
}

static void 
output_header(FILE* f, Node* n)
{
  string header;

  switch (n->type())
    {
    case Node::HA:
      header += "<h1>";
      break;
    case Node::HB:
      header += "<h1>";
      break;
    case Node::HC:
      header += "<h2>";
      header += n->counter().section_str();
      header += ":  ";
      break;
    case Node::HD:
      header += "<h3>";
      header += n->counter().subsection_str();
      header += ":  ";
      break;
    case Node::HE:
      header += "<h4>";
      //      header += n->counter().subsubsection_str();
      //      header += ":  ";
      break;
    default:
      fprintf(stderr, "WARNING: Internal error, node <%s> in %s\n",
              n->name().c_str(), __FUNCTION__);
      return;
      break;
    }

  string content = get_content(n);

  strip_newlines(content);
  strip_leading_trailing(content);

  if (n->type() == Node::HA && content.empty())
    {
      content = n->counter().chapter_str();
    }

  header += content;

  switch (n->type())
    {
    case Node::HA:
      header += "</h1>";
      break;
    case Node::HB:
      header += "</h1>";
      break;
    case Node::HC:
      header += "</h2>";
      break;
    case Node::HD:
      header += "</h3>";
      break;
    case Node::HE:
      header += "</h4>";
      break;
    default:
      fprintf(stderr, "WARNING: Internal error, node <%s> in %s\n",
              n->name().c_str(), __FUNCTION__);
      return;
      break;
    }

  header += "\n";

  html_fputs(header.c_str(), f);
  newline(f);
}

static void 
output_toplevel(FILE* f, Node* n)
{
  switch (n->type())
    {
    case Node::BOOK:
      {
        fprintf(stderr, "Node <%s> unexpected!\n", n->name().c_str());
        return;
      }
      break;

    case Node::COMMENT:
      {
        string content = get_content(n);
        fputs("<!-- ", f);
        fputs(content.c_str(), f);
        fputs("-->\n", f);
      }
      break;
      
    case Node::TITLE:
    case Node::AUTHOR:
    case Node::COPYRIGHT:
      output_frontmatter(f, n);
      return;
      break;
      
    case Node::PART:
      {
        newline(f);
        string partname = "<p><h1>Part " + n->counter().part_str()
          + ": " + get_content(n) + "</h1></p>\n";
        html_fputs(partname.c_str(), f);
        newline(f);
        return;
      }
      break;

    case Node::P:
      output_paragraph(f, n);
      return;
      break;

    case Node::HA:
    case Node::HB:
    case Node::HC:
    case Node::HD:
    case Node::HE:
      output_header(f, n);
      return;
      break;

    case Node::PD:
      {
        newline(f);
        string content = get_content(n);
        strip_newlines(content);
        string pd = "<p><b>PRODUCTION:</b> " + 
          content + string(" </p>\n");
        html_fputs(pd.c_str(), f);
        newline(f);
        return;
      }
      break;

    case Node::QQ:
      {
        // Take this out since people probably don't care
#if 0
        newline(f);
        string content = get_content(n);
        strip_newlines(content);
        string pd = "<p><center><b>QUERY:</b><i> " + content + string("</i></center></p>\n");
        html_fputs(pd.c_str(), f);
        newline(f);
#endif
        return;
      }
      break;

    case Node::CODE:
      {
        // Code can also be within a paragraph
        output_code(f, n, true);
        return;
      }
      break;

    case Node::FUNCLIST:
      output_funclist(f, n);
      break;

    case Node::MACLIST:
      output_maclist(f, n);
      break;

    case Node::TABLE:
      output_table(f, n);
      break;

    case Node::FIGURE:
      output_figure(f, n);
      break;

    case Node::SIDEBAR:
    case Node::FIXME:

      fprintf(stderr, "WARNING: HTML output for <%s> not written yet.\n", 
              n->name().c_str());
      break;

    case Node::CAPTION:
    case Node::IMAGEFILE:
    case Node::TABLEHEAD:
    case Node::TABLEBODY:
    case Node::TABLECELL:
    case Node::MACHEADER:
    case Node::MACSYN:
    case Node::MACNAME:
    case Node::FUNCHEADER:
    case Node::FUNCSYN:
    case Node::FUNCNAME:
    case Node::RETVAL:
    case Node::ARG:
    case Node::ARGTYPE:
    case Node::ARGNAME:
    case Node::VOID:
    case Node::BL:
    case Node::LB:
    case Node::NL:
    case Node::LN:
    case Node::LABEL:
    case Node::CHAPREF:
    case Node::SECREF:
    case Node::FIGREF:
    case Node::TABREF:
    case Node::SIDEREF:
    case Node::INCLUDE:
    case Node::WIDGET:
    case Node::C:
    case Node::FILE:
    case Node::FUNCTION:
    case Node::HEADER:
    case Node::MACRO:
    case Node::MFOUR:
    case Node::MODULE:
    case Node::SIGNAL:
    case Node::TERM:
    case Node::EM:
    case Node::TT:
    case Node::URL:
    case Node::CXX:
    case Node::GLIB:
    case Node::GDK:
    case Node::GTK:
    case Node::GNOME:
    case Node::IMLIB:
    case Node::LINUX:
    case Node::ORBIT:
    case Node::UNIX:
    case Node::TEXT:
      fprintf(stderr, "WARNING: <%s> should not be on the top-level beneath <book>\n", n->name().c_str());
      break;

    default:
      fprintf(stderr, "WARNING: Internal error, node <%s> not handled at toplevel.\n",
              n->name().c_str());
      break;
    }
}

void 
HTML::output(FILE* f, Node* n)
{
  fprintf(f, "<html>\n"
          "  <head>\n"
          "    <META HTTP-EQUIV=\"Content-Type\" CONTENT=\"text/html; charset=iso-8859-1\">\n"
          "    <title>Book</title> \n"
          "  </head> \n"
          "  <BODY TEXT=\"#333333\" BGCOLOR=\"#FFFFFF\" LINK=\"#006699\" VLINK=\"#993300\" ALINK=\"#339900\">"
          "<body>\n");
  
  // Output each node.
  // This is here just so we output in chunks instead of 
  // one massive string.

  const vector<Node*> & children = n->children();
  
  vector<Node*>::const_iterator i = children.begin();

  while (i != children.end())
    {
      output_toplevel(f, *i);
      ++i;
    }


  // Output timestamp.
  struct tm* tm;

  time_t t = time(NULL);

  tm = localtime(&t);

  char buf[128];

  if (tm != 0)
    strftime(buf, 128, "%A, %B %d %Y, %X", tm);
  else 
    strncpy(buf, "(time unknown)", 128);

  fprintf(f, 
          "<p> <small>HTML output machine generated on %s</small>\n",
          buf);

  fprintf(f, "</body></html>\n");
}
