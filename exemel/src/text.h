// -*- C++ -*-
// Copyright 1999 Havoc Pennington. All Rights Reserved.


#ifndef TEXT_H
#define TEXT_H

#include "output.h"

class Text : public Output {
public:
  virtual void output(FILE* f,
                      Node* n);
};

#endif
