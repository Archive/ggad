// -*- C++ -*-
// Copyright 1999 Havoc Pennington. All Rights Reserved.


#include "docbook.h"
#include "tags.h"
#include "util.h"
#include <map>

#include <time.h>
#define _GNU_SOURCE 1
#include <string.h>
#include <stdio.h>

static string get_content(Node* n);

static int next_id = 0;

const char* get_next_id()
{
  static char buf[256];

  snprintf(buf, 256, "z%d", next_id);

  ++next_id;

  return buf;
}

static string
extract_id(Node* n)
{
  // Works on label or ref nodes 
  string id = get_content(n);
  
  unsigned int ind = 0;
  while (ind < id.size())
    {
      if (id[ind] == ':')
        id[ind] = '-';
      else if (id[ind] == '_')
        id[ind] = '-';
      
      ++ind;
    }

  strip_leading_trailing(id);
  
  return id;
}

static string
find_id(Node* n)
{
  string id;

  const vector<Node*> & children = n->children();
  
  vector<Node*>::const_iterator i = children.begin();

  while (i != children.end())
    {
      if ((*i)->type() == Node::LABEL)
        {
          id = extract_id(*i);
          break;
        }

      ++i;
    }

  if (id.empty())
    id = get_next_id();

  return id;
}

// close tags to print
typedef enum {
  CLOSE_SECT3 = 0,
  CLOSE_SECT2 = 1,
  CLOSE_SECT1 = 2,
  CLOSE_CHAPTER = 3,
  CLOSE_PARTINTRO = 4,
  CLOSE_PART = 5,
  CLOSE_BOOK = 6,
  CLOSE_TOPLEVEL = 7
} CloseType;

vector<CloseType> close_stack;

static bool
lessthanequal(CloseType lhs, CloseType rhs)
{
  // chapter and partintro are "equal"
  if (lhs == CLOSE_CHAPTER && rhs == CLOSE_PARTINTRO)
    return true;
  else if (lhs == CLOSE_PARTINTRO && rhs == CLOSE_CHAPTER)
    return true;
  else
    return ((int)lhs) <= ((int)rhs);
}

static void
push_close(FILE* f, CloseType t)
{
  // First close any tags that are "less than or equal" to this one.
  // So for example if we are pushing a Part, then anything smaller
  // than a part, then any previously pending parts, should be closed.

  while (!close_stack.empty())
    {
      CloseType next = close_stack.back();

      if (lessthanequal(next, t))
        {
          switch (next)
            {
            case CLOSE_SECT3:
              fputs("\n</sect3>\n", f);
              break;
              
            case CLOSE_SECT2:
              fputs("\n</sect2>\n", f);
              break;
              
            case CLOSE_SECT1:
              fputs("\n</sect1>\n", f);
              break;
              
            case CLOSE_CHAPTER:
              fputs("\n</chapter>\n", f);
              break;

            case CLOSE_PARTINTRO:
              fputs("\n</partintro>\n", f);
              break;
              
            case CLOSE_PART:
              fputs("\n</part>\n", f);
              break;
              
            case CLOSE_BOOK:
              fputs("\n</book>\n", f);
              break;
              
            default:
              fprintf(stderr, "WARNING: unknown close type in close stack\n");
              break;
            }
          
          close_stack.pop_back();
        }
      else
        break;
    }
  
  close_stack.push_back(t);
  
}

static void 
encode_text_for_docbook(string & content)
{
  // Encode any < and > in the content

  // We have a lame hack in here since the same content can get
  // "encoded" more than once since get_content() is recursive.

  unsigned int j = 0;
  while (j < content.size())
    {
      switch (content[j]) {
      case '<':
        content.replace(j, 1, "&lt;");
        break;
      case '>':
        content.replace(j, 1, "&gt;");
        break;
      case '&':
        // Note that an & we inserted ourselves *this time* is skipped
        // over since it replaces the <, >, or whatever

        // But we have to check that the & doesn't begin a &amp; from a 
        //  previous call.

        if (content.size() - j > 3) 
          {
            if (content.size() - j > 4) // at least four characters left ("amp;")
              {
                if (content[j+1] == 'a' &&
                    content[j+2] == 'm' && 
                    content[j+3] == 'p' &&
                    content[j+4] == ';')
                  {
                    ++j;
                    continue;
                  }
              }
            if (content[j+1] == 'l' &&
                content[j+2] == 't' && 
                content[j+3] == ';')
              {
                ++j;
                continue;
              }
            if (content[j+1] == 'g' &&
                content[j+2] == 't' && 
                content[j+3] == ';')
              {
                ++j;
                continue;
              }
          }

        content.replace(j, 1, "&amp;");
      default:
        break;
      }

      ++j;
    }
}

static void
tag_string(string& s, const string & tag, const char* attrs = 0)
{
  string tmp;

  tmp += "<";
  tmp += tag;
  
  if (attrs)
    {
      tmp += " ";
      tmp += attrs;
    }
  tmp += ">";
  tmp += s;
  tmp += "</";
  tmp += tag;
  tmp += ">";

  s = tmp;
}

static string
tag_node(Node* n, const string & tag, const char* attrs = 0)
{
  string tmp = get_content(n);
  
  strip_newlines(tmp);
  
  encode_text_for_docbook(tmp);
  
  tag_string(tmp, tag, attrs);
  
  return tmp;
}

static void 
html_fputs(const char* s, FILE* f)
{
#if 0
  string enc = s;

  encode_text_for_docbook(enc);
#endif

  fputs(s, f);
}

// Get plain text body content until some other kind of node
// is encountered; increment i accordingly
static string 
get_content_until(vector<Node*>::const_iterator & i, 
                  vector<Node*>::const_iterator end)
{
  string content;

  while (i != end)
    {
      switch ((*i)->type())
        {
        case Node::TEXT:
          {
            string tmp = (*i)->text();
            encode_text_for_docbook(tmp);
            content += tmp;
          }
          break;

        case Node::LABEL:
          {
            // nothing needed
          }
          break;

        case Node::FLREF:
          {
            string id = extract_id(*i);
            content += "<xref role=\"flref\" linkend=\"";
            content += id;
            content += "\">";
#if 0
            content += "<link role=\"flref\" linkend=\"";
            content += id;
            content += "\" endterm=\"" + id + ".title\">";
            content += "</link>";
#endif
          }
          break;

        case Node::MLREF:
          {
            string id = extract_id(*i);

            content += "<xref role=\"mlref\" linkend=\"";
            content += id;
            content += "\">";
            
#if 0
            content += "<link role=\"mlref\" linkend=\"";
            content += id;
            content += "\" endterm=\"" + id + ".title\">";
            content += "</link>";
#endif
          }
          break;
          
        case Node::CHAPREF:
        case Node::SECREF:
        case Node::FIGREF:
        case Node::TABREF:
        case Node::SIDEREF:
          {
            content += "<xref linkend=\"";
            content += extract_id(*i);
            content += "\">";
          }
          break;

        case Node::WIDGET:
          content += tag_node(*i, "classname", "role=\"widget\"");
          break;
          
        case Node::C:
          content += tag_node(*i, "structname", "role=\"C\"");
          break;
          
        case Node::MFOUR:
          content += tag_node(*i, "symbol", "role=\"m4\"");
          break;
          
        case Node::FILE:
          content += tag_node(*i, "filename");
          break;
          
        case Node::HEADER:
          content += tag_node(*i, "filename", "role=\"header\"");
          break;

        case Node::MACRO:
          content += tag_node(*i, "function", "role=\"macro\"");
          break;
          
        case Node::FUNCTION:
          content += tag_node(*i, "function");
          break;

        case Node::MODULE:
          content += tag_node(*i, "application");
          break;
          
        case Node::TT:
          content += tag_node(*i, "application", "role=\"tt\"");
          break;

        case Node::URL:
          {
            //            <ulink url="http://www.gtk.org">GTK+</ulink>
            string tmp = get_content(*i);

            strip_newlines(tmp);

            content += "<ulink url=\"";
            content += tmp;
            content += "\" type=\"http\">";
            content += tmp;
            content += "</ulink>";
          }
          break;          

        case Node::SIGNAL:
          {
            string tmp = get_content(*i);
            
            strip_newlines(tmp);

            tmp = "\"" + tmp + "\"";

            tag_string(tmp, "symbol", "role=\"signal\"");
            
            content += tmp;
          }
          break;

        case Node::TERM:
          content += tag_node(*i, "firstterm");
          break;
          
        case Node::EM:
          content += tag_node(*i, "emphasis");
          break;
          
        case Node::CXX:
          content += "C++";
          break;

        case Node::GLIB:
          content += "glib";
          break;

        case Node::GDK:
          content += "GDK";
          break;

        case Node::GTK:
          content += "GTK+";
          break;

        case Node::GNOME:
          content += "Gnome";
          break;

        case Node::IMLIB:
          content += "Imlib";
          break;

        case Node::LINUX:
          content += "Linux"; 
          break;

        case Node::ORBIT:
          content += "ORBit";
          break;

        case Node::UNIX:
          content += "UNIX";
          break;

        default:
          goto done;
          break;
        };

      ++i;
    }

 done:

  return content;
}

// Consolidate all child nodes; only non-block nodes allowed
static string
get_content(Node* n)
{
  string content;

  const vector<Node*> & children = n->children();
  
  vector<Node*>::const_iterator i = children.begin();

  content = get_content_until(i, children.end());

  if (i != children.end())
    {
      fprintf(stderr, "WARNING: Encountered non-content block nodes in unexpected location.\n");

      while (i != children.end())
        {
          fprintf(stderr, "WARNING:   Unexpected Node <%s> id %u\n",
                  (*i)->name().c_str(), (*i)->id());
          ++i;
        }
    }
  
  return content;
}

static void
output_frontmatter(FILE* f, Node* n)
{
  fprintf(stderr, "WARNING: frontmatter output incorrectly\n");
  
  switch (n->type())
    {
    case Node::TITLE:
      {
        string title;
        title = get_content(n);

        strip_newlines(title);

        newline(f);
        html_fputs("<h1>", f);
        html_fputs(title.c_str(), f);
        html_fputs("</h1>", f);
        newline(f);
        return;
      }
      break;
      
    case Node::AUTHOR:
      {
        string author;
        author = get_content(n);

        strip_newlines(author);

        newline(f);
        html_fputs("<h2>", f);
        html_fputs(author.c_str(), f);
        html_fputs("</h2>", f);
        newline(f);
        return;
      }
      break;

    case Node::COPYRIGHT:
      {
        string copyright;
        copyright = get_content(n);

        strip_newlines(copyright);

        newline(f);
        html_fputs("<h2>", f);
        html_fputs(copyright.c_str(), f);
        html_fputs("</h2>", f);
        newline(f);
      }
      break;

    default:
      fprintf(stderr, "WARNING: Internal error, node <%s> is not front matter\n", n->name().c_str());
      break;
    }
}

static void 
output_code(FILE* f, Node* n, bool toplevel)
{
  if (toplevel)
    newline(f);
  	
  html_fputs("\n<programlisting>\n", f);

  string content = get_content(n);

  html_fputs(content.c_str(), f);

  html_fputs("\n</programlisting>\n", f);

  newline(f);  
  if (toplevel)
    newline(f);
}

static void
output_list(FILE* f, Node* n)
{
  newline(f);
  //  newline(f);

  if (n->type() == Node::BL)
    {
      html_fputs("<itemizedlist>\n", f);
    }
  else if (n->type() == Node::NL)
    {
      html_fputs("<orderedlist>\n", f);
    }

  
  const vector<Node*> & children = n->children();
  
  vector<Node*>::const_iterator i = children.begin();
  vector<Node*>::const_iterator end = children.end();

  while (i != end)
    {
      if (n->type() == Node::BL && (*i)->type() != Node::LB)
        {
          fprintf(stderr, "WARNING: non-bullet immediately inside bullet list\n");
          ++i;
          continue;
        }
      else if (n->type() == Node::NL && (*i)->type() != Node::LN)
        {
          fprintf(stderr, "WARNING: non-number immediately inside number list\n");
          ++i;
          continue;
        }
      
      string content = get_content(*i);

      strip_newlines(content);

      html_fputs("<listitem>\n<para>\n", f);

      html_fputs(content.c_str(), f);

      newline(f);
      html_fputs("\n</para>\n</listitem>\n", f);
      //     newline(f);
      
      ++i;
    }

  if (n->type() == Node::BL)
    {
      html_fputs("\n</itemizedlist>\n", f);
    }
  else if (n->type() == Node::NL)
    {
      html_fputs("\n</orderedlist>\n", f);
    }

  newline(f);
}


static void
get_cells(Node* n, vector<string> & cells)
{
  const vector<Node*> & children = n->children();
  
  vector<Node*>::const_iterator i = children.begin();
  vector<Node*>::const_iterator end = children.end();

  while (i != end)
    {
      string content = get_content(*i);

      cells.push_back(content);

      ++i;
    }
}

static void
output_table(FILE* f, Node* n)
{
  const vector<Node*> & children = n->children();
  
  vector<Node*>::const_iterator i = children.begin();
  vector<Node*>::const_iterator end = children.end();
  
  string caption;
  string id;
  
  vector<string> headers;

  vector<vector<string> > rows;

  while (i != end)
    {
      switch ((*i)->type())
        {
        case Node::CAPTION:
          caption = get_content(*i);
          break;
        case Node::TABLEHEAD:
          {
            get_cells(*i, headers);
          }
          break;
        case Node::TABLEBODY:
          {
            vector<string> row;
            get_cells(*i, row);
            rows.push_back(row);
          }
          break;
        case Node::LABEL:
          id = extract_id(*i);
          break;
        default:
          fprintf(stderr, "WARNING: Node <%s> id %u inside <table>!\n",
                  (*i)->name().c_str(), (*i)->id());
          break;
        }

      ++i;
    }

  string table = "<TABLE COLSEP=\"1\" FRAME=\"all\" ROWSEP=\"0\""
    " SHORTENTRY=\"0\" TOCENTRY=\"1\" TABSTYLE=\"decimalstyle\""
    " ORIENT=\"port\" PGWIDE=\"0\" id=\"" + id + "\">\n";

  table += "<title>" + caption + "</title>\n";
  
  table += "<tgroup align=\"left\" cols=\"";
  char buf[128];
  snprintf(buf, 128, "%d", (int)headers.size());
  table += buf;
  table += "\">\n";

  table += "<thead>\n<row>\n";
  
  vector<string>::iterator h = headers.begin();
  while (h != headers.end())
    {
      tag_string(*h, "entry");
      table += *h;
      ++h;
    }

  table += "\n</row>\n</thead>\n";
  
  table += "\n";

  table += "<tbody>\n";
  
  vector<vector<string> >::iterator vi = rows.begin();
  while (vi != rows.end())
    {
      table += "<row>";
      vector<string>::iterator ri = (*vi).begin();
      while (ri != (*vi).end())
        {
          table += "<entry>";
          table += *ri;
          table += "</entry>";
          ++ri;
        }

      table += "</row>\n";

      ++vi;
    }

  table += "</tbody>\n";
  table += "</tgroup>\n";
  table += "</table>\n";

  html_fputs(table.c_str(), f);
}


static void
output_funcsyn(FILE* f, Node* n)
{
  string content = funcsyn_to_func(n);
  
  strip_leading_trailing(content);

  tag_string(content, "function");

  html_fputs(content.c_str(), f);

#if 0
  const vector<Node*> & children = n->children();
  
  vector<Node*>::const_iterator i = children.begin();
  vector<Node*>::const_iterator end = children.end();
  
  while (i != end)
    {
      string content;
      Node* c = *i;
              
      switch (c->type())
        {
        case Node::FUNCNAME:
          {
            content = get_content(c);
            
            strip_newlines(content);
            strip_leading_trailing(content);

            bool is_broken = false;
            unsigned int x = 0;
            while (x < content.size())
              {
                if (content[x] == '(')
                  {
                    is_broken = true;
                  }

                ++x;
              }
            
            if (is_broken)
              {
                string goodversion = xmlize_c_function(content);

                fprintf(stderr, "WARNING: <funcname> contains the entire function. The proper XML is:\n%s", goodversion.c_str());
              }

            content = "Name: " + content;
          }
          break;

        case Node::RETVAL:
          content = get_content(c);
          
          strip_newlines(content);
          strip_leading_trailing(content);

          content = "Returns: " + content;
          break;

        case Node::ARG:
          {
            content += "Argument: ";

            const vector<Node*> & ch = c->children();
            
            vector<Node*>::const_iterator j = ch.begin();
            vector<Node*>::const_iterator e = ch.end();
            
            while (j != e)
              {
                Node* q = *j;
            
                switch (q->type())
                  {
                  case Node::ARGTYPE:
                    {
                      string t = get_content(q);
                      strip_newlines(t);
                      strip_leading_trailing(t);
                      content += " " + t;
                    }
                    break;
                  case Node::ARGNAME:
                    {
                      string t = get_content(q);
                      strip_newlines(t);
                      strip_leading_trailing(t);
                      content += t;
                    }
                    break;
                    
                  default:
                    fprintf(stderr, "WARNING: <%s> inside an <arg>\n", 
                            q->name().c_str());                    
                    break;
                  }
                ++j;
              }
          }
          break;


        default:
          fprintf(stderr, "WARNING: <%s> inside a <funcsyn>\n", 
                  c->name().c_str());
          break;
        }


      if (!content.empty())
        {
          html_fputs(content.c_str(), f);
          newline(f);
        }

      ++i;
    }
#endif
}

static string
markup_funcsyn(Node* n)
{
  const vector<Node*> & children = n->children();

  string retval;
  string name;
  vector<string> args;
  
  vector<Node*>::const_iterator i = children.begin();
  vector<Node*>::const_iterator end = children.end();
  
  while (i != end)
    {
      string content;
      Node* c = *i;
              
      switch (c->type())
        {
        case Node::FUNCNAME:
          {
            content = get_content(c);
            
            strip_newlines(content);
            strip_leading_trailing(content);

            name = content;
          }
          break;

        case Node::RETVAL:
          content = get_content(c);
          
          strip_newlines(content);
          strip_leading_trailing(content);

          retval = content;
          break;

        case Node::ARG:
          {
            string argtype;
            string argname;
            
            const vector<Node*> & ch = c->children();
            
            vector<Node*>::const_iterator j = ch.begin();
            vector<Node*>::const_iterator e = ch.end();

            while (j != e)
              {
                Node* q = *j;
            
                switch (q->type())
                  {
                  case Node::ARGTYPE:
                    {
                      if (!argtype.empty())
                        fprintf(stderr, "Two argument types?\n");
                      
                      string t = get_content(q);
                      strip_newlines(t);
                      strip_leading_trailing(t);

                      argtype = t;
                    }
                    break;
                  case Node::ARGNAME:
                    {
                      if (!argname.empty())
                        fprintf(stderr, "Two argument names?\n");
                      
                      string t = get_content(q);
                      strip_newlines(t);
                      strip_leading_trailing(t);

                      argname = t;
                    }
                    break;
                    
                  default:
                    fprintf(stderr, "WARNING: <%s> inside an <arg>\n", 
                            q->name().c_str());                    
                    break;
                  }
                ++j;
              }
            
            string argmarkup = "<paramdef>" + argtype + " <parameter>" + argname + "</parameter></paramdef>";

            args.push_back(argmarkup);
          }
          break;


        default:
          fprintf(stderr, "WARNING: <%s> inside a <funcsyn>\n", 
                  c->name().c_str());
          break;
        }
      ++i;
    }
  
  string whole = "<funcprototype>\n<funcdef>";

  whole += retval;
  whole += " <function>";
  whole += name;
  whole += "</function></funcdef>\n";

  if (args.empty())
    {
      whole += "<void>\n";
    }
  else
    {
      vector<string>::iterator ai = args.begin();
      while (ai != args.end())
        {
          whole += *ai;
          
          ++ai;
        }
    }
  
  whole += "</funcprototype>\n";

  return whole;
}

static void
output_funclist(FILE* f, Node* n)
{
  newline(f);

  const vector<Node*> & children = n->children();
  
  vector<Node*>::const_iterator i = children.begin();
  vector<Node*>::const_iterator end = children.end();
  

  vector<string> rows;
  string caption;
  string id;
  
  while (i != end)
    {
      if ((*i)->type() == Node::FUNCSYN)
        {
          string content = markup_funcsyn(*i);
          
          rows.push_back(content);
        }
      else if ((*i)->type() == Node::FUNCHEADER)
        {
          if (!rows.empty())
            fprintf(stderr, "WARNING: <funcheader> should be before any <funcsyn>\n");

          string content = get_content(*i);

          strip_newlines(content);
          strip_leading_trailing(content);


          content = "<funcsynopsisinfo>\n#include &lt;" + content + "&gt;\n</funcsynopsisinfo>\n";
          
          rows.push_back(content);
        }
      else if ((*i)->type() == Node::CAPTION)
        {
          string content = get_content(*i);

          strip_newlines(content);
          strip_leading_trailing(content);

          caption = content;
        }
      else if ((*i)->type() == Node::LABEL)
        {
          id = extract_id(*i);
        }
      else
        {
          fprintf(stderr, "WARNING: Node <%s> in <funclist>\n",
                  (*i)->name().c_str());
        }

      ++i;
    }
  
  string table = "<figure role=\"functionlist\" id=\"" + id +
    "\">\n"; 

  table += "<title id=\"" + id + ".title\">" + caption + "</title>";

  table += "<funcsynopsis role=\"functionlist\" id=\"" + id + ".synopsis\">\n";
  
  vector<string>::iterator si = rows.begin();
  
  while (si != rows.end())
    {
      table += *si;
      ++si;
    }

  table += "</funcsynopsis>\n</figure>\n";
  
  html_fputs(table.c_str(), f);

  newline(f);
}

static void
output_macsyn(FILE* f, Node* n)
{
  const vector<Node*> & children = n->children();
  
  vector<Node*>::const_iterator i = children.begin();
  vector<Node*>::const_iterator end = children.end();
  
  while (i != end)
    {
      string content;
      Node* c = *i;
            
      content = get_content(c);

      strip_leading_trailing(content);
  
      switch (c->type())
        {
        case Node::MACNAME:
          content = "Name: " + content;
          break;
          
        case Node::ARGNAME:
          content = "Arg: " + content;
          break;

        case Node::VOID:
          content = "(no args)";
          break;
          
        default:
          fprintf(stderr, "WARNING: <%s> inside a <macsyn>\n", 
                  c->name().c_str());
          break;
        }


      if (!content.empty())
        {
          html_fputs(content.c_str(), f);
          newline(f);
        }

      ++i;
    }
}

static string
markup_macsyn(Node* n)
{
  const vector<Node*> & children = n->children();

  string retval;
  string name;
  vector<string> args;
  
  vector<Node*>::const_iterator i = children.begin();
  vector<Node*>::const_iterator end = children.end();
  
  while (i != end)
    {
      string content;
      Node* c = *i;
              
      switch (c->type())
        {
        case Node::MACNAME:
          {
            content = get_content(c);
            
            strip_newlines(content);
            strip_leading_trailing(content);

            name = content;
          }
          break;
          
        case Node::ARGNAME:
          {
            string t = get_content(*i);
            strip_newlines(t);
            strip_leading_trailing(t);
            
            string argmarkup = "<paramdef><parameter>" + t + "</parameter></paramdef>";

            args.push_back(argmarkup);
          }
          break;


        case Node::VOID:
          {
            args.push_back("<void>");
          }
          break;
          
        default:
          fprintf(stderr, "WARNING: <%s> inside a <macsyn>\n", 
                  c->name().c_str());
          break;
        }
      ++i;
    }

  string whole;
  
  if (args.empty())
    {
      fprintf(stderr, "Macros are required to have arguments in this markup: %s\n", name.c_str());
      whole += "<symbol>" + name + "</symbol>\n";
    }
  else
    {
      whole = "<funcprototype>\n<funcdef>";
      
      whole += retval;
      whole += " <function role=\"macro\">";
      whole += name;
      whole += "</function></funcdef>\n";
      
      vector<string>::iterator ai = args.begin();
      while (ai != args.end())
        {
          whole += *ai;
          
          ++ai;
        }
      whole += "</funcprototype>\n";
    }

  return whole;
}

static void
output_maclist(FILE* f, Node* n)
{
  newline(f);

  const vector<Node*> & children = n->children();
  
  vector<Node*>::const_iterator i = children.begin();
  vector<Node*>::const_iterator end = children.end();

  vector<string> rows;
  string caption;
  string id;
  
  while (i != end)
    {
      if ((*i)->type() == Node::MACSYN)
        {
          string content = markup_macsyn(*i);
          
          rows.push_back(content);
        }
      else if ((*i)->type() == Node::MACHEADER)
        {
          if (!rows.empty())
            fprintf(stderr, "WARNING: <macheader> should be before any <macsyn>\n");

          string content = get_content(*i);

          strip_newlines(content);
          strip_leading_trailing(content);

          content = "<funcsynopsisinfo>\n#include &lt;" + content + "&gt;\n</funcsynopsisinfo>\n";
          
          rows.push_back(content);
        }
      else if ((*i)->type() == Node::CAPTION)
        {
          string content = get_content(*i);

          strip_newlines(content);
          strip_leading_trailing(content);

          caption = content;
        }
      else if ((*i)->type() == Node::LABEL)
        {
          id = extract_id(*i);
        }
      else
        {
          fprintf(stderr, "WARNING: Node <%s> in <maclist>\n",
                  (*i)->name().c_str());
        }

      ++i;
    }

  string table = "<figure role=\"macrolist\" id=\"" + id +
    "\">\n"; 

  table += "<title id=\"" + id + ".title\">" + caption + "</title>";

  table += "<funcsynopsis role=\"macrolist\" id=\"" + id + ".synopsis\">\n";
  
  vector<string>::iterator si = rows.begin();
  
  while (si != rows.end())
    {
      table += *si;
      ++si;
    }

  table += "</funcsynopsis>\n</figure>\n";
  
  html_fputs(table.c_str(), f);

  newline(f);
}


static void
output_figure(FILE* f, Node* n)
{
  const vector<Node*> & children = n->children();
  
  vector<Node*>::const_iterator i = children.begin();
  vector<Node*>::const_iterator end = children.end();

  string caption;
  string file;

  while (i != end)
    {
      switch ((*i)->type())
        {
        case Node::CAPTION:
          {
            caption = get_content(*i);
          }
          break;
          
        case Node::IMAGEFILE:
          {
            string content = get_content(*i);
            strip_leading_trailing(content);
            file = content;
          }
          break;
          
        case Node::LABEL:
          break;

        default:
          fprintf(stderr, "WARNING: Node <%s> inside <figure>\n", (*i)->name().c_str());
          break;
          
        }

      ++i;
    }

  
  string all = "<figure float=\"1\" id=\"";
  all += find_id(n);
  all += "\">\n<title>";
  all += caption;
  all += "</title>\n";
  all += "<graphic fileref=\"";
  if (file.size() > 4)
    file.replace(file.size()-4, 4, ""); // strip .png
  all += file;
  all += "\" format=\"png\"></graphic>\n</figure>";
  
  html_fputs(all.c_str(), f);
}

static void
output_paragraph(FILE* f, Node* n)
{
  const vector<Node*> & children = n->children();
  
  vector<Node*>::const_iterator i = children.begin();
  vector<Node*>::const_iterator end = children.end();

  while (i != end)
    {
      string content;
            
      content = get_content_until(i, end);

      strip_newlines(content);
      strip_leading_trailing(content);

      if (!content.empty())
        {
          html_fputs("<para> ", f);
          html_fputs(content.c_str(), f);
          newline(f);
          html_fputs("</para> ", f);
          newline(f);
        }

      if (i != end)
        {
          Node* c = *i;
          // non-content node, do the appropriate thing
          switch (c->type())
            {
            case Node::CODE:
              output_code(f, c, false);
              break;

            case Node::BL:
            case Node::NL:
              output_list(f, c);
              break;

            case Node::FUNCLIST:
              output_funclist(f, c);
              break;

            case Node::MACLIST:
              output_maclist(f, c);
              break;

            case Node::TABLE:
              output_table(f, c);
              break;

            case Node::P:
            case Node::CAPTION:
            case Node::IMAGEFILE:
            case Node::TABLEHEAD:
            case Node::TABLEBODY:
            case Node::TABLECELL:
            case Node::MACHEADER:
            case Node::MACSYN:
            case Node::MACNAME:
            case Node::FUNCHEADER:
            case Node::FUNCSYN:
            case Node::FUNCNAME:
            case Node::RETVAL:
            case Node::ARG:
            case Node::ARGTYPE:
            case Node::ARGNAME:
            case Node::VOID:
            case Node::LB:
            case Node::LN:
            case Node::LABEL:
            case Node::CHAPREF:
            case Node::SECREF:
            case Node::FIGREF:
            case Node::TABREF:
            case Node::SIDEREF:
            case Node::MLREF:
            case Node::FLREF:
            case Node::INCLUDE:
            case Node::WIDGET:
            case Node::C:
            case Node::FILE:
            case Node::FUNCTION:
            case Node::HEADER:
            case Node::MACRO:
            case Node::MFOUR:
            case Node::MODULE:
            case Node::SIGNAL:
            case Node::TERM:
            case Node::EM:
            case Node::TT:
            case Node::URL:
            case Node::CXX:
            case Node::GLIB:
            case Node::GDK:
            case Node::GTK:
            case Node::GNOME:
            case Node::IMLIB:
            case Node::LINUX:
            case Node::ORBIT:
            case Node::UNIX:
            case Node::TEXT:
              fprintf(stderr, "WARNING: <%s> id %u should not be below <p>\n", c->name().c_str(), c->id());
              break;

            default:
              fprintf(stderr, "WARNING: Internal error, node <%s> id %u not handled in paragraph.\n",
                      c->name().c_str(), c->id());
              break;
            }

          ++i;
        }
    }
}

static void 
output_header(FILE* f, Node* n)
{
  string id = find_id(n);
  
  string header;
  
  switch (n->type())
    {
    case Node::HA:
      return; // DocBook does it automatically 
      break;
    case Node::HB:
      push_close(f, CLOSE_CHAPTER);
      header += "<chapter id=\"";
      header += id;
      header += "\">";
      break;
    case Node::HC:
      push_close(f, CLOSE_SECT1);
      header += "<sect1 id=\"";
      header += id;
      header += "\">";
      break;
    case Node::HD:
      push_close(f, CLOSE_SECT2);
      header += "<sect2 id=\"";
      header += id;
      header += "\">";
      break;
    case Node::HE:
      push_close(f, CLOSE_SECT3);
      header += "<sect3 id=\"";
      header += id;
      header += "\">";

      break;
    default:
      fprintf(stderr, "WARNING: Internal error, node <%s> in %s\n",
              n->name().c_str(), __FUNCTION__);
      return;
      break;
    }

  string content = get_content(n);

  strip_newlines(content);
  strip_leading_trailing(content);

  header += "\n<title>";
  header += content;
  header += "</title>\n\n";

  html_fputs(header.c_str(), f);
  newline(f);
}

static void 
output_toplevel(FILE* f, Node* n)
{
  switch (n->type())
    {
    case Node::BOOK:
      {
        fprintf(stderr, "Node <%s> unexpected!\n", n->name().c_str());
        return;
      }
      break;

    case Node::TITLE:
    case Node::AUTHOR:
    case Node::COPYRIGHT:
      // We'll do this by hand
      //      output_frontmatter(f, n);
      return;
      break;
      
    case Node::PART:
      {
        push_close(f, CLOSE_PART);
        newline(f);
        string partname = "<part id=\"";
        partname += find_id(n);
        partname += "\"><title>" + get_content(n) + "</title>\n";
        html_fputs(partname.c_str(), f);
        newline(f);
        push_close(f, CLOSE_PARTINTRO);
        fputs("<partintro>\n", f);
        return;
      }
      break;

    case Node::P:
      output_paragraph(f, n);
      return;
      break;

    case Node::HA:
    case Node::HB:
    case Node::HC:
    case Node::HD:
    case Node::HE:
      output_header(f, n);
      return;
      break;

    case Node::PD:
      {
#if 0
        newline(f);
        string content = get_content(n);
        strip_newlines(content);
        string pd = "<p><b>PRODUCTION:</b> " + 
          content + string(" </p>\n");
        html_fputs(pd.c_str(), f);
        newline(f);
#endif
        return;
      }
      break;

    case Node::QQ:
      {
        // Take this out since people probably don't care
#if 0
        newline(f);
        string content = get_content(n);
        strip_newlines(content);
        string pd = "<p><center><b>QUERY:</b><i> " + content + string("</i></center></p>\n");
        html_fputs(pd.c_str(), f);
        newline(f);
#endif
        return;
      }
      break;

    case Node::CODE:
      {
        // Code can also be within a paragraph
        output_code(f, n, true);
        return;
      }
      break;

    case Node::FUNCLIST:
      output_funclist(f, n);
      break;

    case Node::MACLIST:
      output_maclist(f, n);
      break;

    case Node::TABLE:
      output_table(f, n);
      break;

    case Node::FIGURE:
      output_figure(f, n);
      break;

    case Node::SIDEBAR:
    case Node::FIXME:

      fprintf(stderr, "WARNING: DocBook output for <%s> not written yet.\n", 
              n->name().c_str());
      break;

    case Node::CAPTION:
    case Node::IMAGEFILE:
    case Node::TABLEHEAD:
    case Node::TABLEBODY:
    case Node::TABLECELL:
    case Node::MACHEADER:
    case Node::MACSYN:
    case Node::MACNAME:
    case Node::FUNCHEADER:
    case Node::FUNCSYN:
    case Node::FUNCNAME:
    case Node::RETVAL:
    case Node::ARG:
    case Node::ARGTYPE:
    case Node::ARGNAME:
    case Node::VOID:
    case Node::BL:
    case Node::LB:
    case Node::NL:
    case Node::LN:
    case Node::LABEL:
    case Node::CHAPREF:
    case Node::SECREF:
    case Node::FIGREF:
    case Node::TABREF:
    case Node::SIDEREF:
    case Node::INCLUDE:
    case Node::WIDGET:
    case Node::C:
    case Node::FILE:
    case Node::FUNCTION:
    case Node::HEADER:
    case Node::MACRO:
    case Node::MFOUR:
    case Node::MODULE:
    case Node::SIGNAL:
    case Node::TERM:
    case Node::EM:
    case Node::TT:
    case Node::URL:
    case Node::CXX:
    case Node::GLIB:
    case Node::GDK:
    case Node::GTK:
    case Node::GNOME:
    case Node::IMLIB:
    case Node::LINUX:
    case Node::ORBIT:
    case Node::UNIX:
    case Node::TEXT:
      fprintf(stderr, "WARNING: <%s> should not be on the top-level beneath <book>\n", n->name().c_str());
      break;

    default:
      fprintf(stderr, "WARNING: Internal error, node <%s> not handled at toplevel.\n",
              n->name().c_str());
      break;
    }
}

void 
DocBook::output(FILE* f, Node* n)
{
  fprintf(f, "<!doctype book PUBLIC \"-//GNOME//DTD DocBook PNG Variant V1.0//EN\" [\n]>\n<book id=\"GGAD\">\n");
  
  // Output each node.
  // This is here just so we output in chunks instead of 
  // one massive string.

  const vector<Node*> & children = n->children();
  
  vector<Node*>::const_iterator i = children.begin();

  while (i != children.end())
    {
      output_toplevel(f, *i);
      ++i;
    }

  push_close(f, CLOSE_BOOK);
  
  fprintf(f, "</book>\n");
}
