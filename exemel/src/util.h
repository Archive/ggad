// -*- C++ -*-
// Copyright 1999 Havoc Pennington. All Rights Reserved.


#ifndef UTIL_H
#define UTIL_H

#include <stdio.h>
#include <string>
#include <vector>

void strip_leading_trailing(string & s);

void center_in(string & s, size_t columns);

void newline(FILE* f);

void paragraphize(string & s);

void strip_newlines(string & s);

char* file_to_string(const string & filename);

void parse_c_function(const string & text,
                      string& rettype, 
                      string& funcname,
                      vector<string> & argtypes,
                      vector<string> & argnames);

string xmlize_c_function(const string & text);

#endif
