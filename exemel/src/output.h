// -*- C++ -*-
// Copyright 1999 Havoc Pennington. All Rights Reserved.


#ifndef OUTPUT_H
#define OUTPUT_H

#include <stdio.h>
#include "node.h"

class Output {
public:
  virtual void output(FILE* f,
                      Node* n) = 0;
};

#endif
