// -*- C++ -*-


#ifndef COUNTER_H
#define COUNTER_H

#include <string>
#include <map>
#include "util.h"

class Counter {
public:
  Counter();

  // default copy constructor is fine

  int part() const;
  int chapter() const;
  int section() const; 
  int subsection() const;
  int subsubsection() const;
  int subsubsubsection() const;
  int table() const;
  int figure() const;
  int maclist() const;
  int funclist() const; 
 
  void inc_part();
  void inc_chapter();
  void inc_section(); 
  void inc_subsection();
  void inc_subsubsection();
  void inc_subsubsubsection();
  void inc_table();
  void inc_figure();
  void inc_funclist();
  void inc_maclist();

  string part_str() const { 
    return int_str(part_);
  }
  
  string chapter_str() const {
    return int_str(chapter_);
  }

  string section_str() const {
    if (subsection_ != 0) 
      return subsection_str();
    else 
      return int_str(chapter_) + "." + int_str(section_);
  }

  string subsection_str() const {
    if (subsubsection_ != 0)
      return subsubsection_str();
    else 
      return int_str(chapter_) + "." + 
        int_str(section_) + "."  + 
        int_str(subsection_);
  }

  string subsubsection_str() const {
    if (subsubsubsection_ != 0)
      return subsubsubsection_str();
    else
      return  int_str(chapter_) + "." + 
        int_str(section_) + "."  + 
        int_str(subsection_); // + "." + 
    //int_str(subsubsection_);
  }

  string subsubsubsection_str() const {
    return  int_str(chapter_) + "." + 
      int_str(section_) + "."  + 
      int_str(subsection_); // + "." + 
    //      int_str(subsubsection_) + "." + 
    //      int_str(subsubsubsection_);
  }

  string table_str() const {
    return int_str(chapter_) + "." + int_str(table_);
  }

  string figure_str() const {
    return int_str(chapter_) + "." + int_str(figure_);
  }

  string maclist_str() const {
    return int_str(chapter_) + "." + int_str(maclist_);
  }

  string funclist_str() const {
    return int_str(chapter_) + "." + int_str(funclist_);
  }

private:
  int part_;
  int chapter_;
  int section_;
  int subsection_;
  int subsubsection_;
  int subsubsubsection_;

  int table_;
  int figure_;  

  int maclist_;
  int funclist_;

  string int_str(int i) const {
    char buf[128];
    sprintf(buf, "%d", i);
    return string(buf);
  }
};

class Refs {
public:
  const Counter* lookup(const string & label);

  void insert(const string& label, 
              const Counter& counter);

private:
  map<string,Counter*> refs_;
};

Refs & get_refs();

#endif
