// -*- C++ -*-
// Copyright 1999 Havoc Pennington. All Rights Reserved.


#ifndef DOCBOOK_H
#define DOCBOOK_H

#include "output.h"

class DocBook : public Output {
public:
  virtual void output(FILE* f,
                      Node* n);
};

#endif
