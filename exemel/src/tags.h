// -*- C++ -*-
// Copyright 1999 Havoc Pennington. All Rights Reserved.


#ifndef TAGS_H
#define TAGS_H

#define BOOK_T "book"
#define ULIST_T "ul"
#define BULLET_T "lb"
#define TITLE_T "title"
#define HEAD_A_T "ha"
#define HEAD_B_T "hb"
#define HEAD_C_T "hc"
#define HEAD_D_T "hd"
#define HEAD_E_T "he"
#define PAR_T "p"
#define PART_T "part"
#define WIDGET_T "widget"

#endif
