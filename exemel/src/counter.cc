// -*- C++ -*-


#include "counter.h"

Counter::Counter()
  : part_(0), chapter_(0), section_(0),
    subsection_(0), subsubsection_(0), 
    subsubsubsection_(0), table_(0), 
    figure_(0)
{

}

int
Counter::part() const
{
  return part_;
}

int
Counter::chapter() const
{
  return chapter_;
}

int
Counter::section() const
{
  return section_;
}

int
Counter::subsection() const
{
  return subsection_;
}

int
Counter::subsubsection() const
{
  return subsubsection_;
}

int
Counter::subsubsubsection() const
{
  return subsubsubsection_;
}

int
Counter::table() const
{
  return table_;
}

int
Counter::figure() const
{
  return figure_;
}

int
Counter::maclist() const 
{
  return maclist_;
}
 
int
Counter::funclist() const 
{
  return funclist_;
}

void
Counter::inc_part()
{
  ++part_;  
}

void
Counter::inc_chapter()
{
  ++chapter_;

  section_ = subsection_ = subsubsection_ = subsubsubsection_ = 0;
  table_ = figure_ = maclist_ = funclist_ = 0;
}

void
Counter::inc_section()
{
  ++section_;
  subsection_ = subsubsection_ = subsubsubsection_ = 0;
}

void
Counter::inc_subsection()
{
  ++subsection_;
  subsubsection_ = subsubsubsection_ = 0;
}

void
Counter::inc_subsubsection()
{
  ++subsubsection_; 
  subsubsubsection_ = 0;
}

void
Counter::inc_subsubsubsection()
{
  ++subsubsubsection_;
}

void
Counter::inc_table()
{
  ++table_;
}

void
Counter::inc_figure()
{
  ++figure_;
}

void
Counter::inc_maclist()
{
  ++maclist_;
}

void
Counter::inc_funclist()
{
  ++funclist_;
}

const Counter* 
Refs::lookup(const string & label)
{
  map<string,Counter*>::iterator i = refs_.find(label);

  if (i != refs_.end())
    {
      //      printf("Resolved reference %s\n", label.c_str());
      return i->second;
    }
  else
    return 0;
}

void 
Refs::insert(const string& label, 
             const Counter& counter)
{
  // yet another memory leak! woo hoo
  refs_[label] = new Counter(counter);

  //  printf("Added reference %s\n", label.c_str());
}

static Refs r;

Refs & 
get_refs()
{
  return r;
}

