
#include "util.h"

#include <unistd.h>
#include <sys/stat.h>
#include <errno.h>

void 
strip_leading_trailing(string & s)
{
  // Strip leading whitespace
  size_t i = 0;
  while (i < s.size() && isspace(s[i]))
    ++i;

  if (i > 0)
    s.erase(s.begin(), s.begin() + i);

  // Strip trailing whitespace
  if (s.size() > 0)
    {
      i = s.size();

      do 
        {
          --i;          
        }
      while (i > 0 && isspace(s[i]));

      ++i; // think about it

      s.erase(s.begin() + i, s.end());
    }
}


void 
remove_all_whitespace(string & s)
{
  unsigned int i = 0;
  while (i < s.size())
    {
      if (isspace(s[i]))
        s.erase(s.begin() + i, s.begin() + i + 1);
      else 
        ++i;
    }
}

void 
center_in(string & s, size_t columns)
{
  if (s.size() > columns)
    fprintf(stderr, "WARNING: centered text doesn't fit in %u columns.\n",
            columns);
    
  size_t diff = columns - s.size();

  size_t half = diff/2;

  string pad;
  while (half > 0)
    {
      pad += ' ';
      --half;
    }

  s = pad + s;
}

void 
newline(FILE* f)
{
  fputc('\n', f);
}

void 
paragraphize(string & s)
{
  strip_leading_trailing(s);

  string r;
  size_t column = 0;

  string::iterator i = s.begin();
  while (i != s.end())
    {
      if (isblank(*i))
        {
          if (column != 0) 
            {
              r += ' '; // add a single space
              ++column;
            }

          ++i;

          // slurp the rest
          while (i != s.end() && isblank(*i))
            ++i;
        }
      
      if (i != s.end())
        {
          r += *i;

          if (*i == '\n') 
            column = 0;
          else 
            ++column;

          ++i;
        }
    }

  s = r;
}

void 
strip_newlines(string & s)
{
  string clean;

  unsigned int i = 0;
  while (i < s.size())
    {
      if (s[i] != '\n')
        clean += s[i];
      else 
        {
          if (i != (s.size()-1))
            {
              clean += ' '; // replace newline with space if it's not the end
            }
        }

      ++i;
    }

  s = clean;
}

char* 
file_to_string(const string & filename)
{
  FILE* fp = 0;
  
  struct stat buf;
  
  if (stat(filename.c_str(), &buf) == -1)
    {
      fprintf(stderr, "Failure on `%s': %s\n",
              filename.c_str(), strerror(errno));
      exit(1);
    }
  
  fp = fopen(filename.c_str(), "r");
  
  if (fp == 0)
    {
      fprintf(stderr, "Failed to open `%s': %s\n",
              filename.c_str(), strerror(errno));
      exit(1);
    }
  
  const size_t max = buf.st_size*2+100; // paranoid size
  char* mem = (char*)malloc(max);
  memset(mem, '\0', max);
  
  // not very robust here.
  fread(mem, sizeof(char), max, fp);
  
  if (ferror(fp))
    {
      fprintf(stderr, "Failed to read `%s': %s\n",
              filename.c_str(), strerror(errno));
      exit(1);
    }

  return mem;
}

void 
parse_c_function(const string & text,
                 string& rettype, 
                 string& funcname,
                 vector<string> & argtypes,
                 vector<string> & argnames)
{
  string uptoparen;
  string args;

  bool to_args = false;

  unsigned int i = 0;
  
  while (i < text.size())
    {
      if (text[i] == '(')
        to_args = true;

      if (!to_args)
        uptoparen += text[i];
      else 
        args += text[i];

      ++i;
    }

  strip_leading_trailing(uptoparen);
  strip_leading_trailing(args);

  //  printf("uptoparen: `%s'\n", uptoparen.c_str());
  //  printf("args: `%s'\n", args.c_str());

  unsigned int j = uptoparen.size();
  while (j > 0)
    {
      --j;

      if (uptoparen[j] == ' ' || uptoparen[j] == '*')
        {
          ++j;
          break;
        }
    }

  rettype = string(uptoparen, 0, j);

  funcname = string(uptoparen, j, (uptoparen.size()-j));

  strip_leading_trailing(rettype);
  strip_leading_trailing(funcname);
  
  //  printf("rettype: `%s'\n", rettype.c_str());
  //  printf("name: `%s'\n", funcname.c_str());

  vector<string> eachone;

  j = 0;
  while (j < args.size())
    {
      string thisone;
      
      while (j < args.size())
        {
          if (args[j] == '(' || args[j] == ',' || args[j] == ')')
            {
              ++j;
              break;
            }

          thisone += (args[j]);
          
          ++j;
        }

      if (!thisone.empty())
        {
          eachone.push_back(thisone);
          //          printf("one arg: `%s'\n", thisone.c_str());
        }
    }

  vector<string>::iterator q = eachone.begin();

  while (q != eachone.end())
    {
      string one = *q;

      strip_leading_trailing(one);

      j = one.size();
      while (j > 0)
        {
          --j;
 
          if (one[j] == ' ' || one[j] == '*')
            {
              ++j;
              break;
            }
        }

      string t = string(one, 0, j);
      
      string n = string(one, j, (one.size()-j));

      strip_leading_trailing(t);
      strip_leading_trailing(n);
  
      //remove_all_whitespace(t);
      //remove_all_whitespace(n);

      //      printf("argtype: `%s'\n", t.c_str());
      //      printf("argname: `%s'\n", n.c_str());

      argtypes.push_back(t);
      argnames.push_back(n);

      ++q;
    }
}

string 
xmlize_c_function(const string & text)
{
  string rettype; 
  string funcname;
  vector<string> argtypes;
  vector<string> argnames;

  parse_c_function(text,
                   rettype, 
                   funcname, 
                   argtypes,
                   argnames);

  string xml;

  xml += "<funcsyn>\n";
  xml += "  <retval>";
  xml += rettype;
  xml += "</retval>\n";

  xml += "  <funcname>";
  xml += funcname;
  xml += "</funcname>\n";

  vector<string>::iterator i = argtypes.begin();  
  vector<string>::iterator j = argnames.begin();
  
  while (i != argtypes.end() && j != argnames.end())
    {
      xml += "  <arg>\n";
      if (!(*i).empty()) // the ... case has no type, only name
        {
          xml += "    <argtype>";
          xml += *i;
          xml += "</argtype>\n";
        }
      xml += "    <argname>";
      xml += *j;
      xml += "</argname>\n";
      xml += "  </arg>\n";

      ++i;
      ++j;
    }

  xml += "</funcsyn>\n";

  return xml;
}

