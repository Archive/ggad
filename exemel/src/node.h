// -*- C++ -*-
// Copyright 1999 Havoc Pennington. All Rights Reserved.


#ifndef NODE_H
#define NODE_H

#include <stdio.h>
#include <gnome-xml/parser.h>
#include <string>
#include <vector>
#include <counter.h>

class Node {
public:
  typedef enum {
    BOOK,
    TITLE,
    AUTHOR,
    COPYRIGHT,

    PART,
    
    P,

    HA,
    HB,
    HC,
    HD,
    HE,
    
    BL,
    LB,
    NL,
    LN,
    
    PD,
    
    QQ,

    CODE,
    SIDEBAR,
    FIGURE,
    IMAGEFILE,
    TABLE,
    CAPTION,
    TABLEHEAD,
    TABLEBODY,
    TABLECELL,
    FIXME,

    FUNCLIST,
    FUNCHEADER,
    FUNCSYN,
    FUNCNAME,
    RETVAL,
    ARG,
    ARGTYPE,
    ARGNAME,
    VOID,

    MACLIST,
    MACHEADER,
    MACSYN,
    MACNAME,
    
    LABEL,
    CHAPREF,
    SECREF,
    FIGREF,
    TABREF,
    SIDEREF,
    MLREF,
    FLREF,
    INCLUDE,
    
    WIDGET,
    C,
    FILE,
    FUNCTION,
    HEADER,
    MACRO,
    MFOUR,
    MODULE,
    SIGNAL,
    TERM,
    TT,
    EM,
    URL,

    CXX,
    GLIB,
    GDK,
    GTK,
    GNOME,
    IMLIB,
    LINUX,
    ORBIT,
    UNIX,
    
    // This isn't a tag; it corresponds to text contained inside a
    // tag, with the entities etc. resolved.
    TEXT,

    // This is an XML comment <!-- -->
    COMMENT,
    
    END_TYPE

  } Type;

  Type type() const { return type_; }

  Node();

  void set(xmlNodePtr xml);

  const string& name() const { return name_; }

  // Is it a new "text block" or just a differently-formatted word or two
  bool block() const { return block_; }

  const vector<Node*> & children() { return children_; }

  Node* parent() { return parent_; }
  
  void add(Node* n);

  void spew(::FILE* f, int depth);

  unsigned int id() const { return nodeid_; }

  const string& text() const { return text_; }

  Counter& counter() { return counter_; }

  void clear_children() { children_.clear(); } // leak memory! whee!

  //protected:
  void set_parent(Node* n) { 
    if (parent_ != 0) 
      fprintf(stderr, "Attempt to re-set parent!\n");
    parent_ = n; 
  }

  void set_type(Type t) { type_ = t; }

  void set_text(const string & t) { text_ = t; }

  void set_name(const string & n) { name_ = n; }

  static xmlNodePtr make_text_node(xmlNodePtr tmp, Node* parent);

private:
  Type type_;
  Node* parent_;
  vector<Node*> children_;
  xmlNodePtr xml_;
  string name_;
  string text_; 
  unsigned int nodeid_;
  Counter counter_;
  unsigned int block_ : 1;
};

void postprocess_tree(Node* n, Counter & c, bool tableize);

const Counter* get_ref(Node* n);
string get_ref_string(Node* n);


string funcsyn_to_func(Node* n, bool format=true);
string macsyn_to_mac(Node* n);

#endif
