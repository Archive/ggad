// -*- C++ -*-
// Copyright 1999 Havoc Pennington. All Rights Reserved.


#include "text.h"
#include "tags.h"
#include "util.h"
#include <map>

#include <time.h>

static const size_t ncols = 80;

static void
encode_monospace(string & s)
{
  s = "%%" + s + "%%";
}

static void
encode_italics(string & s)
{
  s = "~~" + s + "~~";
}

static string get_content(Node* n);

// Get plain text body content until some other kind of node
// is encountered; increment i accordingly
static string 
get_content_until(vector<Node*>::const_iterator & i, 
                  vector<Node*>::const_iterator end)
{
  string content;

  while (i != end)
    {
      switch ((*i)->type())
        {
        case Node::TEXT:
          {
            content += (*i)->text();
          }
          break;

        case Node::LABEL:
          {
            // nothing needed
          }
          break;

        case Node::CHAPREF:
          {
            content += "Chapter ";
            const Counter* c = get_ref(*i);
            content += c ? c->chapter_str() : string("[FIXME: Chapter number]");
            if (c == 0)
              fprintf(stderr, "WARNING: reference `%s' is undefined.\n",
                      get_content(*i).c_str());
          }
          break;

        case Node::SECREF:
          {
            content += "Section ";
            const Counter* c = get_ref(*i);
            content += c ? c->section_str() : string("[FIXME: Section number]");
            if (c == 0)
              fprintf(stderr, "WARNING: reference `%s' is undefined.\n",
                      get_content(*i).c_str());
          }
          break;
            
        case Node::FIGREF:
          {
            content += "Figure ";
            const Counter* c = get_ref(*i);
            content += c ? c->figure_str() : string("[FIXME: Figure number]");
            if (c == 0)
              fprintf(stderr, "WARNING: reference `%s' is undefined.\n",
                      get_content(*i).c_str());
          }
          break;

        case Node::TABREF:
          {
            content += "Table ";
            const Counter* c = get_ref(*i);
            content += c ? c->table_str() : string("[FIXME: Table number]");
            if (c == 0)
              fprintf(stderr, "WARNING: reference `%s' is undefined.\n",
                      get_content(*i).c_str());
          }
          break;

        case Node::SIDEREF:
          {
            content += "Sidebar ";
            content += "XXX";
          }
          break;


        case Node::MLREF:
          {
            content += "Macro Listing ";
            const Counter* c = get_ref(*i);
            content += c ? c->maclist_str() : string("[FIXME: Macro Listing number]");
            if (c == 0)
              fprintf(stderr, "WARNING: reference `%s' is undefined.\n",
                      get_content(*i).c_str());
          }
          break;

        case Node::FLREF:
          {
            content += "Function Listing ";
            const Counter* c = get_ref(*i);
            content += c ? c->funclist_str() : string("[FIXME: Function Listing number]");
            if (c == 0)
              fprintf(stderr, "WARNING: reference `%s' is undefined.\n",
                      get_content(*i).c_str());
          }
          break;

        case Node::WIDGET:
        case Node::C:
        case Node::FILE:
        case Node::FUNCTION:
        case Node::HEADER:
        case Node::MACRO:
        case Node::MFOUR:
        case Node::MODULE:
        case Node::TT:
        case Node::URL:
          {
            string tmp = get_content(*i);

            strip_newlines(tmp);

            encode_monospace(tmp);

            content += tmp;
          }
          break;


        case Node::SIGNAL:
          {
            string tmp = get_content(*i);
            
            strip_newlines(tmp);

            tmp = "\"" + tmp + "\"";

            encode_monospace(tmp);
            
            content += tmp;
          }
          break;

        case Node::TERM:
        case Node::EM:
          {
            string tmp = get_content(*i);

            strip_newlines(tmp);

            encode_italics(tmp);

            content += tmp;
          }
          break;
          
        case Node::CXX:
          content += "C++";
          break;

        case Node::GLIB:
          content += "glib";
          break;

        case Node::GDK:
          content += "Gdk";
          break;

        case Node::GTK:
          content += "Gtk+";
          break;

        case Node::GNOME:
          content += "Gnome";
          break;

        case Node::IMLIB:
          content += "Imlib";
          break;

        case Node::LINUX:
          content += "Linux"; 
          break;

        case Node::ORBIT:
          content += "ORBit";
          break;

        case Node::UNIX:
          content += "UNIX";
          break;

        default:
          goto done;
          break;
        };

      ++i;
    }

 done:

  return content;
}

// Consolidate all child nodes; only non-block nodes allowed
static string
get_content(Node* n)
{
  string content;

  const vector<Node*> & children = n->children();
  
  vector<Node*>::const_iterator i = children.begin();

  content = get_content_until(i, children.end());

  if (i != children.end())
    {
      fprintf(stderr, "WARNING: Encountered non-content block nodes in unexpected location.\n");

      while (i != children.end())
        {
          fprintf(stderr, "WARNING:   Unexpected Node <%s> id %u\n",
                  (*i)->name().c_str(), (*i)->id());
          ++i;
        }
    }
  
  return content;
}

static void
output_frontmatter(FILE* f, Node* n)
{
  switch (n->type())
    {
    case Node::TITLE:
      {
        string title;
        title = get_content(n);

        strip_newlines(title);

        center_in(title, ncols);

        newline(f);
        fputs(title.c_str(), f);
        newline(f);
        return;
      }
      break;
      
    case Node::AUTHOR:
      {
        string author;
        author = get_content(n);

        strip_newlines(author);

        center_in(author, ncols);

        newline(f);
        fputs(author.c_str(), f);
        newline(f);
        return;
      }
      break;

    case Node::COPYRIGHT:
      {
        string copyright;
        copyright = get_content(n);

        strip_newlines(copyright);

        center_in(copyright, ncols);

        newline(f);
        fputs(copyright.c_str(), f);
        newline(f);
      }
      break;

    default:
      fprintf(stderr, "WARNING: Internal error, node <%s> is not front matter\n", n->name().c_str());
      break;
    }
}

static void 
output_code(FILE* f, Node* n, bool toplevel)
{
  if (toplevel) newline(f);
  fputs("\n%%\n", f);

  string content = get_content(n);

  fputs(content.c_str(), f);

  fputs("\n%%\n", f);
  newline(f);  
  if (toplevel) newline(f);
}

static void
output_list(FILE* f, Node* n)
{
  newline(f);
  //  newline(f);
  
  const vector<Node*> & children = n->children();
  
  vector<Node*>::const_iterator i = children.begin();
  vector<Node*>::const_iterator end = children.end();

  unsigned int num = 1;

  while (i != end)
    {
      if (n->type() == Node::BL && (*i)->type() != Node::LB)
        {
          fprintf(stderr, "WARNING: non-bullet immediately inside bullet list\n");
          ++i;
          continue;
        }
      else if (n->type() == Node::NL && (*i)->type() != Node::LN)
        {
          fprintf(stderr, "WARNING: non-number immediately inside number list\n");
          ++i;
          continue;
        }
      
      string content = get_content(*i);

      strip_newlines(content);

      if (n->type() == Node::BL)
        {
          fputs("\n   [1b] ", f);
        }
      else if (n->type() == Node::NL)
        {
          char buf[128];
          snprintf(buf, 128, "\n   %u ", num);
          fputs(buf, f);

          ++num;
        }
      else 
        {
          fprintf(stderr, "WARNING: Internal error, non-list in output_list()\n");
        }

      fputs(content.c_str(), f);

      newline(f);
      //     newline(f);
      
      ++i;
    }

  newline(f);
}

static void
output_funcsyn(FILE* f, Node* n)
{
  const vector<Node*> & children = n->children();
  
  vector<Node*>::const_iterator i = children.begin();
  vector<Node*>::const_iterator end = children.end();
  
  while (i != end)
    {
      string content;
      Node* c = *i;
              
      switch (c->type())
        {
        case Node::FUNCNAME:
          {
            content = get_content(c);
            
            strip_newlines(content);
            strip_leading_trailing(content);

            bool is_broken = false;
            unsigned int x = 0;
            while (x < content.size())
              {
                if (content[x] == '(')
                  {
                    is_broken = true;
                  }

                ++x;
              }
            
            if (is_broken)
              {
                string goodversion = xmlize_c_function(content);

                fprintf(stderr, "WARNING: <funcname> contains the entire function. The proper XML is:\n%s", goodversion.c_str());
              }

            content = "Name: " + content;
          }
          break;

        case Node::RETVAL:
          content = get_content(c);
          
          strip_newlines(content);
          strip_leading_trailing(content);

          content = "Returns: " + content;
          break;

        case Node::ARG:
          {
            content += "Argument: ";

            const vector<Node*> & ch = c->children();
            
            vector<Node*>::const_iterator j = ch.begin();
            vector<Node*>::const_iterator e = ch.end();
            
            while (j != e)
              {
                Node* q = *j;
            
                switch (q->type())
                  {
                  case Node::ARGTYPE:
                    {
                      string t = get_content(q);
                      strip_newlines(t);
                      strip_leading_trailing(t);
                      content += " " + t;
                    }
                    break;
                  case Node::ARGNAME:
                    {
                      string t = get_content(q);
                      strip_newlines(t);
                      strip_leading_trailing(t);
                      content += t;
                    }
                    break;
                    
                  default:
                    fprintf(stderr, "WARNING: <%s> inside an <arg>\n", 
                            q->name().c_str());                    
                    break;
                  }
                ++j;
              }
          }
          break;


        default:
          fprintf(stderr, "WARNING: <%s> inside a <funcsyn>\n", 
                  c->name().c_str());
          break;
        }


      if (!content.empty())
        {
          fputs(content.c_str(), f);
          newline(f);
        }

      ++i;
    }
}

static void
output_funclist(FILE* f, Node* n)
{
  newline(f);

  const vector<Node*> & children = n->children();
  
  vector<Node*>::const_iterator i = children.begin();
  vector<Node*>::const_iterator end = children.end();
  
  vector<string> rows;
  string caption;

  bool had_caption = false;
  bool had_header = false;
  bool had_label = false;

  while (i != end)
    {
      if ((*i)->type() == Node::FUNCSYN)
        {
          string content = funcsyn_to_func(*i);
          
          strip_leading_trailing(content);

          encode_monospace(content);

          content += "\n";

          rows.push_back(content);
        }
      else if ((*i)->type() == Node::FUNCHEADER)
        {
          if (!rows.empty())
            fprintf(stderr, "WARNING: <funcheader> should be before any <funcsyn>\n");

          string content = get_content(*i);

          strip_newlines(content);
          strip_leading_trailing(content);

          content = "#include <" + content + ">";

          encode_monospace(content);
          
          content += "\n";

          had_header = true;

          rows.push_back(content);
        }
      else if ((*i)->type() == Node::CAPTION)
        {
          string content = get_content(*i);

          strip_newlines(content);
          strip_leading_trailing(content);

          had_caption = true;

          caption = "Function Listing " + (*i)->counter().funclist_str() + ": " + content + "\n";
        }
      else if ((*i)->type() == Node::LABEL)
        {
          had_label = true;
          // Always ignored - processed earlier by the counter thing
        }
      else
        {
          fprintf(stderr, "WARNING: Node <%s> in <funclist>\n",
                  (*i)->name().c_str());
        }

      ++i;
    }

  bool say_which = false;

  if (!had_caption || !had_header || !had_label)
    say_which = true;

  if (!had_caption)
    fprintf(stderr, "WARNING: no caption for funclist\n");
  if (!had_header)
    fprintf(stderr, "WARNING: no header for funclist\n");
  if (!had_label)
    fprintf(stderr, "WARNING: no label for funclist\n");

  fputs(caption.c_str(), f);
  if (say_which)
    fputs(caption.c_str(), stderr);

  vector<string>::iterator si = rows.begin();
  while (si != rows.end())
    {
      fputs((*si).c_str(), f);
      if (say_which)
        fputs((*si).c_str(), stderr);
      ++si;
    }

  newline(f);
}

static void
output_macsyn(FILE* f, Node* n)
{
  const vector<Node*> & children = n->children();
  
  vector<Node*>::const_iterator i = children.begin();
  vector<Node*>::const_iterator end = children.end();
  
  while (i != end)
    {
      string content;
      Node* c = *i;
            
      content = get_content(c);

      strip_newlines(content);
      strip_leading_trailing(content);
  
      switch (c->type())
        {
        case Node::MACNAME:
          content = "Name: " + content;
          break;
          
        case Node::ARGNAME:
          content = "Arg: " + content;
          break;

        case Node::VOID:
          content = "(no args)";
          break;
          
        default:
          fprintf(stderr, "WARNING: <%s> inside a <macsyn>\n", 
                  c->name().c_str());
          break;
        }


      if (!content.empty())
        {
          fputs(content.c_str(), f);
          newline(f);
        }

      ++i;
    }
}

static void
output_maclist(FILE* f, Node* n)
{
  newline(f);

  const vector<Node*> & children = n->children();
  
  vector<Node*>::const_iterator i = children.begin();
  vector<Node*>::const_iterator end = children.end();

  vector<string> rows;
  string caption;

  bool had_caption = false;
  bool had_header = false;
  bool had_label = false;

  while (i != end)
    {
      if ((*i)->type() == Node::MACSYN)
        {
          string content = macsyn_to_mac(*i);
          
          strip_leading_trailing(content);

          encode_monospace(content);

          content += "\n";

          rows.push_back(content);
        }
      else if ((*i)->type() == Node::MACHEADER)
        {
          if (!rows.empty())
            fprintf(stderr, "WARNING: <macheader> should be before any <macsyn>\n");

          string content = get_content(*i);

          strip_newlines(content);
          strip_leading_trailing(content);

          content = "#include <" + content + ">";

          encode_monospace(content);
          
          content += "\n";

          had_header = true;

          rows.push_back(content);
        }
      else if ((*i)->type() == Node::CAPTION)
        {
          string content = get_content(*i);

          strip_newlines(content);
          strip_leading_trailing(content);

          had_caption = true;

          caption = "Macro Listing " + (*i)->counter().maclist_str() + ": " + content + "\n";
        }
      else if ((*i)->type() == Node::LABEL)
        {
          had_label = true;
          // Always ignored - processed earlier by the counter thing
        }
      else
        {
          fprintf(stderr, "WARNING: Node <%s> in <maclist>\n",
                  (*i)->name().c_str());
        }

      ++i;
    }

  bool say_which = false;

  if (!had_caption || !had_header || !had_label)
    say_which = true;

  if (!had_caption)
    fprintf(stderr, "WARNING: no caption for maclist\n");
  if (!had_header)
    fprintf(stderr, "WARNING: no header for maclist\n");
  if (!had_label)
    fprintf(stderr, "WARNING: no label for maclist\n");


  fputs(caption.c_str(), f);
  if (say_which)
    fputs(caption.c_str(), stderr);

  vector<string>::iterator si = rows.begin();
  while (si != rows.end())
    {
      fputs((*si).c_str(), f);
      if (say_which)
        fputs((*si).c_str(), stderr);
      ++si;
    }

  newline(f);
}

static void
get_cells(Node* n, vector<string> & cells)
{
  const vector<Node*> & children = n->children();
  
  vector<Node*>::const_iterator i = children.begin();
  vector<Node*>::const_iterator end = children.end();

  while (i != end)
    {
      string content = get_content(*i);

      cells.push_back(content);

      ++i;
    }
}

static void
output_table(FILE* f, Node* n)
{
  const vector<Node*> & children = n->children();
  
  vector<Node*>::const_iterator i = children.begin();
  vector<Node*>::const_iterator end = children.end();
  
  string caption;

  vector<string> headers;

  vector<vector<string> > rows;

  while (i != end)
    {
      switch ((*i)->type())
        {
        case Node::CAPTION:
          caption = get_content(*i);
          break;
        case Node::TABLEHEAD:
          {
            get_cells(*i, headers);
          }
          break;
        case Node::TABLEBODY:
          {
            vector<string> row;
            get_cells(*i, row);
            rows.push_back(row);
          }
          break;
        case Node::LABEL:
          break;
        default:
          fprintf(stderr, "WARNING: Node <%s> inside <table>!\n",
                  (*i)->name().c_str());
          break;
        }

      ++i;
    }

  string table;
  table += "[TC] Table " + n->counter().table_str() + "[md]" + 
    caption + "\n";

  table += "[TH] ";

  vector<string>::iterator h = headers.begin();
  while (h != headers.end())
    {
      table += "   ";
      table += *h;      
      ++h;
    }

  table += "\n";

  vector<vector<string> >::iterator vi = rows.begin();
  while (vi != rows.end())
    {
      table += "[TB] ";
      vector<string>::iterator ri = (*vi).begin();
      while (ri != (*vi).end())
        {
          table += "   ";
          table += *ri;
          ++ri;
        }

      table += "\n";

      ++vi;
    }

  fputs(table.c_str(), f);
  newline(f);
}

static void
output_paragraph(FILE* f, Node* n)
{
  const vector<Node*> & children = n->children();
  
  vector<Node*>::const_iterator i = children.begin();
  vector<Node*>::const_iterator end = children.end();

  while (i != end)
    {
      string content;
            
      content = get_content_until(i, end);

      strip_newlines(content);
      strip_leading_trailing(content);

      if (!content.empty())
        {
          fputs("[FT] ", f);
          fputs(content.c_str(), f);
          newline(f);
          newline(f);
        }

      if (i != end)
        {
          Node* c = *i;
          // non-content node, do the appropriate thing
          switch (c->type())
            {
            case Node::CODE:
              output_code(f, c, false);
              break;

            case Node::BL:
            case Node::NL:
              output_list(f, c);
              break;

            case Node::FUNCLIST:
              output_funclist(f, c);
              break;

            case Node::MACLIST:
              output_maclist(f, c);
              break;

            case Node::TABLE:
              output_table(f, c);
              break;

            case Node::P:
            case Node::CAPTION:
            case Node::IMAGEFILE:
            case Node::TABLEHEAD:
            case Node::TABLEBODY:
            case Node::TABLECELL:
            case Node::MACHEADER:
            case Node::MACSYN:
            case Node::MACNAME:
            case Node::FUNCHEADER:
            case Node::FUNCSYN:
            case Node::FUNCNAME:
            case Node::RETVAL:
            case Node::ARG:
            case Node::ARGTYPE:
            case Node::ARGNAME:
            case Node::VOID:
            case Node::LB:
            case Node::LN:
            case Node::LABEL:
            case Node::CHAPREF:
            case Node::SECREF:
            case Node::FIGREF:
            case Node::TABREF:
            case Node::SIDEREF:
            case Node::MLREF:
            case Node::FLREF:
            case Node::INCLUDE:
            case Node::WIDGET:
            case Node::C:
            case Node::FILE:
            case Node::FUNCTION:
            case Node::HEADER:
            case Node::MACRO:
            case Node::MFOUR:
            case Node::MODULE:
            case Node::SIGNAL:
            case Node::TERM:
            case Node::EM:
            case Node::TT:
            case Node::URL:
            case Node::CXX:
            case Node::GLIB:
            case Node::GDK:
            case Node::GTK:
            case Node::GNOME:
            case Node::IMLIB:
            case Node::LINUX:
            case Node::ORBIT:
            case Node::UNIX:
            case Node::TEXT:
              fprintf(stderr, "WARNING: <%s> id %u should not be below <p>\n", c->name().c_str(), c->id());
              break;

            default:
              fprintf(stderr, "WARNING: Internal error, node <%s> id %u not handled in paragraph.\n",
                      c->name().c_str(), c->id());
              break;
            }

          ++i;
        }
    }
}

static void 
output_header(FILE* f, Node* n)
{
  string header;

  switch (n->type())
    {
    case Node::HA:
      header += "[HA] ";
      break;
    case Node::HB:
      header += "[HB] ";
      break;
    case Node::HC:
      header += "[HC]   ";
      header += n->counter().section_str();
      header += ": ";
      break;
    case Node::HD:
      header += "[HD]      ";
      header += n->counter().subsection_str();
      header += ": ";
      break;
    case Node::HE:
      header += "[HE]        ";
      //      header += n->counter().subsubsection_str();
      //      header += ": ";
      break;
    default:
      fprintf(stderr, "WARNING: Internal error, node <%s> in %s\n",
              n->name().c_str(), __FUNCTION__);
      return;
      break;
    }

  string content = get_content(n);

  strip_newlines(content);

  strip_leading_trailing(content);

  if (n->type() == Node::HA && content.empty())
    {
      content = n->counter().chapter_str();
    }

  header += content;
  header += "\n";

  fputs(header.c_str(), f);
  newline(f);
}

static void
output_figure(FILE* f, Node* n)
{
  const vector<Node*> & children = n->children();
  
  vector<Node*>::const_iterator i = children.begin();
  vector<Node*>::const_iterator end = children.end();

  while (i != end)
    {
      switch ((*i)->type())
        {
        case Node::CAPTION:
          {
            string content = get_content(*i);
            string disp = "  *** Figure " + (*i)->counter().figure_str() + ": " + content + " ***\n";
            fputs(disp.c_str(), f);
          }
          break;
          
        case Node::IMAGEFILE:
          {
            string content = get_content(*i);
            string disp = "  *** Figure " + (*i)->counter().figure_str() + " is in file " + content + " ***\n";
            fputs(disp.c_str(), f);          
          }
          break;
          
        case Node::LABEL:
          break;

        default:
          fprintf(stderr, "WARNING: Node <%s> inside <figure>\n", (*i)->name().c_str());
          break;
          
        }

      ++i;
    }
  newline(f);
}

static void 
output_toplevel(FILE* f, Node* n)
{
  switch (n->type())
    {
    case Node::BOOK:
      {
        fprintf(stderr, "Node <%s> unexpected!\n", n->name().c_str());
        return;
      }
      break;

    case Node::TITLE:
    case Node::AUTHOR:
    case Node::COPYRIGHT:
      output_frontmatter(f, n);
      return;
      break;
      
    case Node::PART:
      {
        newline(f);
        string partname = "Part " + n->counter().part_str() + ": " + 
          get_content(n) + "\n";
        center_in(partname, ncols);
        fputs(partname.c_str(), f);
        newline(f);
        return;
      }
      break;

    case Node::P:
      output_paragraph(f, n);
      return;
      break;

    case Node::HA:
    case Node::HB:
    case Node::HC:
    case Node::HD:
    case Node::HE:
      output_header(f, n);
      return;
      break;

    case Node::PD:
      {
        newline(f);
        string content = get_content(n);
        strip_newlines(content);
        string pd = "[PD] " + string("*** ") + 
          content + string(" ***\n");
        fputs(pd.c_str(), f);
        newline(f);
        return;
      }
      break;

    case Node::QQ:
      {
        newline(f);
        string content = get_content(n);
        strip_newlines(content);
        string pd = "[QQ] " + content + string("\n");
        fputs(pd.c_str(), f);
        newline(f);
        return;
      }
      break;

    case Node::CODE:
      {
        // Code can also be within a paragraph
        output_code(f, n, true);
        return;
      }
      break;

    case Node::FUNCLIST:
      output_funclist(f, n);
      break;

    case Node::MACLIST:
      output_maclist(f, n);
      break;

    case Node::TABLE:
      output_table(f, n);
      break;

    case Node::FIGURE:
      output_figure(f, n);
      break;

    case Node::SIDEBAR:
    case Node::FIXME:

      fprintf(stderr, "WARNING: Text output for <%s> not written yet.\n", 
              n->name().c_str());
      break;

    case Node::CAPTION:
    case Node::IMAGEFILE:
    case Node::TABLEHEAD:
    case Node::TABLEBODY:
    case Node::TABLECELL:
    case Node::MACHEADER:
    case Node::MACSYN:
    case Node::MACNAME:
    case Node::FUNCHEADER:
    case Node::FUNCSYN:
    case Node::FUNCNAME:
    case Node::RETVAL:
    case Node::ARG:
    case Node::ARGTYPE:
    case Node::ARGNAME:
    case Node::VOID:
    case Node::BL:
    case Node::LB:
    case Node::NL:
    case Node::LN:
    case Node::LABEL:
    case Node::CHAPREF:
    case Node::SECREF:
    case Node::FIGREF:
    case Node::TABREF:
    case Node::SIDEREF:
    case Node::INCLUDE:
    case Node::WIDGET:
    case Node::C:
    case Node::FILE:
    case Node::FUNCTION:
    case Node::HEADER:
    case Node::MACRO:
    case Node::MFOUR:
    case Node::MODULE:
    case Node::SIGNAL:
    case Node::TERM:
    case Node::EM:
    case Node::TT:
    case Node::URL:
    case Node::CXX:
    case Node::GLIB:
    case Node::GDK:
    case Node::GTK:
    case Node::GNOME:
    case Node::IMLIB:
    case Node::LINUX:
    case Node::ORBIT:
    case Node::UNIX:
    case Node::TEXT:
      fprintf(stderr, "WARNING: <%s> should not be on the top-level beneath <book>\n", n->name().c_str());
      break;

    default:
      fprintf(stderr, "WARNING: Internal error, node <%s> not handled at toplevel.\n",
              n->name().c_str());
      break;
    }
}

void 
Text::output(FILE* f, Node* n)
{
  // Output timestamp.
  struct tm* tm;

  time_t t = time(NULL);

  tm = localtime(&t);

  char buf[128];

  if (tm != 0)
    strftime(buf, 128, "%A, %B %d %Y, %X", tm);
  else 
    strncpy(buf, "(time unknown)", 128);

  fprintf(f, 
          "Text output machine generated on %s\n",
          buf);

  // Output each node.
  // This is here just so we output in chunks instead of 
  // one massive string.

  const vector<Node*> & children = n->children();
  
  vector<Node*>::const_iterator i = children.begin();

  while (i != children.end())
    {
      output_toplevel(f, *i);
      ++i;
    }

  // Output The End line
  newline(f);
  string theend = "The End";
  center_in(theend, ncols);
  fprintf(f, "%s\n", theend.c_str());
  newline(f);
}
