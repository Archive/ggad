// -*- C++ -*-
// Copyright 1999 Havoc Pennington. All Rights Reserved.

#include "node.h"
#include "util.h"
#include <map>
#include <unistd.h>
#include <sys/stat.h>
#include <errno.h>

void printmem(char* mem, size_t n)
{
  size_t i = 0;
  while (i < n)
    {
      char buf[2];
      buf[0] = mem[i] ? mem[i] : '_';
      buf[1] = '\0';
      printf("%s", buf);
      ++i;
    }
  printf("\n");
}

class NameHash {
public:
  NameHash();

  Node::Type get_type(const string & name);

  static bool is_block(Node::Type t);

private:
  // it isn't a hash at all, but humor me.
  map<string,Node::Type> hash_;
};

NameHash::NameHash()
{
  hash_["book"] = Node::BOOK;
  hash_["title"] = Node::TITLE;
  hash_["author"] = Node::AUTHOR;
  hash_["copyright"] = Node::COPYRIGHT;
  hash_["part"] = Node::PART;

  hash_["p"] = Node::P;

  hash_["ha"] = Node::HA;
  hash_["hb"] = Node::HB;
  hash_["hc"] = Node::HC;
  hash_["hd"] = Node::HD;
  hash_["he"] = Node::HE;

  hash_["bl"] = Node::BL;
  hash_["lb"] = Node::LB;
  hash_["nl"] = Node::NL;
  hash_["ln"] = Node::LN;

  hash_["pd"] = Node::PD;

  hash_["qq"] = Node::QQ;

  hash_["code"] = Node::CODE;
  hash_["sidebar"] = Node::SIDEBAR;
  hash_["figure"] = Node::FIGURE;
  hash_["imagefile"] = Node::IMAGEFILE;
  hash_["table"] = Node::TABLE;
  hash_["caption"] = Node::CAPTION;
  hash_["tablehead"] = Node::TABLEHEAD;
  hash_["tablebody"] = Node::TABLEBODY;
  hash_["tablecell"] = Node::TABLECELL;
  hash_["fixme"] = Node::FIXME;

  hash_["funclist"] = Node::FUNCLIST;
  hash_["funcheader"] = Node::FUNCHEADER;
  hash_["funcsyn"] = Node::FUNCSYN;
  hash_["funcname"] = Node::FUNCNAME;
  hash_["retval"] = Node::RETVAL;
  hash_["arg"] = Node::ARG;
  hash_["argtype"] = Node::ARGTYPE;
  hash_["argname"] = Node::ARGNAME;
  hash_["void"] = Node::VOID;

  hash_["maclist"] = Node::MACLIST;
  hash_["macheader"] = Node::MACHEADER;
  hash_["macsyn"] = Node::MACSYN;
  hash_["macname"] = Node::MACNAME;

  hash_["label"] = Node::LABEL;
  hash_["chapref"] = Node::CHAPREF;
  hash_["secref"] = Node::SECREF;
  hash_["figref"] = Node::FIGREF;
  hash_["tabref"] = Node::TABREF;
  hash_["sideref"] = Node::SIDEREF;
  hash_["mlref"] = Node::MLREF;
  hash_["flref"] = Node::FLREF;
  hash_["include"] = Node::INCLUDE;

  hash_["widget"] = Node::WIDGET;
  hash_["c"] = Node::C;
  hash_["file"] = Node::FILE;
  hash_["function"] = Node::FUNCTION;
  hash_["header"] = Node::HEADER;
  hash_["macro"] = Node::MACRO;
  hash_["mfour"] = Node::MFOUR;
  hash_["module"] = Node::MODULE;
  hash_["signal"] = Node::SIGNAL;
  hash_["term"] = Node::TERM;
  hash_["tt"] = Node::TT;
  hash_["em"] = Node::EM;
  hash_["url"] = Node::URL;

  hash_["cxx"] = Node::CXX;
  hash_["glib"] = Node::GLIB;
  hash_["gdk"] = Node::GDK;
  hash_["gtk"] = Node::GTK;
  hash_["gnome"] = Node::GNOME;
  hash_["imlib"] = Node::IMLIB;
  hash_["linux"] = Node::LINUX;
  hash_["orbit"] = Node::ORBIT;
  hash_["unix"] = Node::UNIX;
}

bool 
NameHash::is_block(Node::Type t)
{
  switch (t)
    {
    case Node::BOOK:
    case Node::TITLE:
    case Node::AUTHOR:
    case Node::COPYRIGHT:
    case Node::PART:
    case Node::P:
    case Node::HA:
    case Node::HB:
    case Node::HC:
    case Node::HD:
    case Node::HE:
    case Node::BL:
    case Node::LB:
    case Node::NL:
    case Node::LN:
    case Node::PD:
    case Node::QQ:
    case Node::CODE:
    case Node::SIDEBAR:
    case Node::FIGURE:
    case Node::TABLE:
    case Node::CAPTION:
    case Node::TABLEHEAD:
    case Node::TABLEBODY:
    case Node::TABLECELL:
    case Node::FIXME:
    case Node::FUNCLIST:
      return true;
      break;

    case Node::FUNCHEADER:
    case Node::FUNCSYN:
    case Node::FUNCNAME:
    case Node::RETVAL:
    case Node::ARG:
    case Node::ARGTYPE:
    case Node::ARGNAME:
    case Node::VOID:
    case Node::MACLIST:
    case Node::MACHEADER:
    case Node::MACSYN:
    case Node::MACNAME:
    case Node::LABEL:
    case Node::CHAPREF:
    case Node::SECREF:
    case Node::FIGREF:
    case Node::TABREF:
    case Node::SIDEREF:
    case Node::MLREF:
    case Node::FLREF:
    case Node::INCLUDE:
    case Node::IMAGEFILE:
    case Node::WIDGET:
    case Node::C:
    case Node::FILE:
    case Node::FUNCTION:
    case Node::HEADER:
    case Node::MACRO:
    case Node::MFOUR:
    case Node::MODULE:
    case Node::SIGNAL:
    case Node::TERM:
    case Node::TT:
    case Node::EM:
    case Node::URL:
    case Node::CXX:
    case Node::GLIB:
    case Node::GDK:
    case Node::GTK:
    case Node::GNOME:
    case Node::IMLIB:
    case Node::LINUX:
    case Node::ORBIT:
    case Node::UNIX:
    case Node::COMMENT:
      return false;
      break;
    
   default:
     fprintf(stderr, "WARNING: Internal error, don't know blockiness of type.\n");
     return false;
      break;
    }
}

Node::Type
NameHash::get_type(const string & name)
{
  map<string,Node::Type>::const_iterator i = hash_.find(name);

  if (i != hash_.end()) return i->second;
  else return Node::END_TYPE;
}

static NameHash namehash;

static unsigned int nodeid = 0;

Node::Node()
  : type_(END_TYPE),
    parent_(0),
    xml_(0),
    nodeid_(nodeid),
    block_(false)
{
  ++nodeid;
}

// walk tmp until a non-text node is found, create text Node, then
// return next node
xmlNodePtr 
Node::make_text_node(xmlNodePtr tmp, Node* parent)
{
  string text;
  
  while (tmp && 
         (tmp->type == XML_TEXT_NODE || 
          tmp->type == XML_ENTITY_REF_NODE ||
          tmp->type == XML_COMMENT_NODE))
    {
      if (tmp->type == XML_TEXT_NODE) 
        {
          text += string((const char*)tmp->content);
        } 
      else if (tmp->type == XML_ENTITY_REF_NODE) 
        {
          xmlEntityPtr ent = xmlGetDocEntity(tmp->doc, tmp->name);
          if (ent != 0)
            text += ent->content ? (const char*)ent->content : "";
          else
            text += tmp->content ? (const char*)tmp->content : "";
        }
      tmp = tmp->next;
    }
  
  if (!text.empty())
    {
      Node* child = new Node;
      child->set_type(TEXT);
      child->set_text(text);

      size_t len = text.length() > 15 ? 15 : text.length();
      string unescaped = text.substr(0, len);
      string escaped = "`";

      size_t i = 0;
      while (i < len)
        {
          if (unescaped[i] == '\n')
            escaped += "\\n";
          else 
            escaped += unescaped[i];

          ++i;
        }
      escaped += "'";

      string name = "TEXT: " + escaped;

      child->set_name(name);
      parent->add(child);
    }

  return tmp;
}

void
Node::set(xmlNodePtr xml)
{
  if (xml == 0)
    {
      fprintf(stderr, "WARNING: Internal error: NULL xmlNodePtr, I'm confused now.\n");
      return;
    }

  switch (xml->type)
    {
    case XML_COMMENT_NODE:
      fprintf(stderr, "WARNING: supposed to ignore comment nodes\n");
      name_ = "!-- --";
      type_ = COMMENT;
      return;
      break;
      
    case XML_DOCUMENT_FRAG_NODE:
    case XML_ELEMENT_NODE:
      break;
    default:
      fprintf(stderr, "WARNING: Internal error, setting Node from wrong xmlNodePtr type %d.\n", xml->type);
      return;
    }

  if (type_ != COMMENT && xml->name == 0)
    {
      fprintf(stderr, "WARNING: Internal error, NULL name for xmlNodePtr\n");
      return;
    }

  if (type_ != COMMENT)
    {
      string name = (const char*)xml->name;

      type_ = namehash.get_type(name);

      if (type_ == END_TYPE)
        {
          fprintf(stderr, "WARNING: unknown tag <%s>\n", name.c_str());
          name_ = "UNKNOWN: ";
          name_ += name;
          return;
        }
      name_ = name;
    }
      
  if (type_ == INCLUDE)
    {

    }

  // At this point we are NOT an include file and NOT an unknown node type
  
  xml_ = xml;
  block_ = NameHash::is_block(type_);

  xmlNodePtr tmp = xml_->childs;
  while (tmp)
    {
      tmp = make_text_node(tmp, this);
      
      if (tmp)
        {
          switch (tmp->type)
            {
            case XML_DOCUMENT_FRAG_NODE:
            case XML_ELEMENT_NODE:
              {
                if (xmlStrcmp(tmp->name, (const CHAR*)"include") == 0)
                  {
                    string filename;

                    xmlNodePtr tmp3 = tmp->childs;

                    while (tmp3 && 
                           (tmp3->type == XML_TEXT_NODE || 
                            tmp3->type == XML_ENTITY_REF_NODE ||
                            tmp3->type == XML_COMMENT_NODE))
                      {
                        if (tmp3->type == XML_TEXT_NODE) 
                          {
                            filename += string((const char*)tmp3->content);
                          } 
                        else if (tmp3->type == XML_ENTITY_REF_NODE) 
                          {
                            xmlEntityPtr ent = xmlGetDocEntity(tmp3->doc, tmp3->name);
                            if (ent != 0)
                              filename += ent->content ? (const char*)ent->content : "";
                            else
                              filename += tmp3->content ? (const char*)tmp3->content : "";
                          }
                        tmp3 = tmp3->next;
                      }

                    strip_leading_trailing(filename);

                    // this is a sort of progress display.
                    //printf("%s ", filename.c_str());
                    fputs(".", stdout);
                    fflush(stdout);

                    // We need to replace this node with whatever is in the include file
                    ::FILE* fp = 0;

                    struct stat buf;
      
                    if (stat(filename.c_str(), &buf) == -1)
                      {
                        fprintf(stderr, "Failure on `%s': %s\n",
                                filename.c_str(), strerror(errno));
                        exit(1);
                      }

                    fp = fopen(filename.c_str(), "r");

                    if (fp == 0)
                      {
                        fprintf(stderr, "Failed to open `%s': %s\n",
                                filename.c_str(), strerror(errno));
                        exit(1);
                      }
      
                    const size_t max = buf.st_size*2+100; // paranoid size
                    char* mem = new char[max];
                    memset(mem, '\0', max);

                    // Put dummy tags at the start and end of the buffer
                    char* start = mem;
                    char* contents = mem + sizeof("<dummy>\n") - 1;

                    memcpy(start, "<dummy>\n", sizeof("<dummy>\n"));

                    size_t bytes_read = fread(contents, sizeof(char), max, fp);

                    char* end = contents+bytes_read;
                    strcpy(end,   "\n</dummy>\n");
                    end += sizeof("\n</dummy>\n");
      
                    if (ferror(fp))
                      {
                        fprintf(stderr, "Failed to read `%s': %s\n",
                                filename.c_str(), strerror(errno));
                        exit(1);
                      }

                    size_t toparse = end-start;

                    //                    printmem(start, toparse);

                    xmlDocPtr doc = xmlParseMemory(start, toparse);

                    // doc->root is the <dummy> node
      
                    if (doc != 0 && doc->root != 0)
                      {
                        if (doc->root->childs != 0)
                          {
                            
                            // Add file contents, starting with this node (replacing <include>)
                            //  and creating new sibling nodes as needed.
                            
                            
                            xmlNodePtr tmp2 = doc->root->childs;

                            while (tmp2)
                            {
                              tmp2 = make_text_node(tmp2, this);
                              if (tmp2)
                                {
                                  if (tmp2->type != XML_COMMENT_NODE)
                                    {
                                      Node* n = new Node;
                                      n->set(tmp2);
                                      add(n);
                                    }
                                  tmp2 = tmp2->next;
                                }
                            }
                          }
                      }
                    else 
                      {
                        fprintf(stderr, "Failed to parse included file `%s'.\n", filename.c_str());
                        exit(1);
                      }
                  }
                else // If it isn't an include
                  {
                    Node* child = new Node;
                    child->set(tmp);
                    add(child);
                  }
              }
              break;
            default:
              break;
            };
          
          tmp = tmp->next;
        }
    }
}

void 
Node::add(Node* n)
{
  if (n == 0)
    {
      fprintf(stderr, "WARNING: Internal error (NULL node added)\n");
      return;
    }

  if (type_ == END_TYPE)
    {
      fprintf(stderr, "WARNING: tag <%s> inside unknown tag <%s>\n", 
              n->name().c_str(), name_.c_str());
      goto error;
    }

  switch (type_)
    {
    case BOOK:
      {
      }
      break;


    case TITLE:
    case AUTHOR:
    case COPYRIGHT:
    case PART:
      {
        if (n->block())
          {
            fprintf(stderr, "WARNING: text block tag <%s> not allowed in <%s>\n", n->name().c_str(), name_.c_str());
            goto error;
          }
      }
      break;
    
    case P:
      {
      }
      break;



    case HA:
      {
        if (n->type() != TEXT)
          {
            fprintf(stderr, "WARNING: tag <%s> not allowed in <ha>\n", n->name().c_str());
            goto error;
          }
      }
      break;


    case HB:
      {
        if (n->block())
          {
            fprintf(stderr, "WARNING: text block tag <%s> not allowed in <hb>\n", n->name().c_str());
            goto error;
          }
      }
      break;


    case HC:
      {
        if (n->block())
          {
            fprintf(stderr, "WARNING: text block tag <%s> not allowed in <hc>\n", n->name().c_str());
            goto error;
          }
      }
      break;


    case HD:
      {
        if (n->block())
          {
            fprintf(stderr, "WARNING: text block tag <%s> not allowed in <hd>\n", n->name().c_str());
            goto error;
          }
      }
      break;


    case HE:
      {
        if (n->block())
          {
            fprintf(stderr, "WARNING: text block tag <%s> not allowed in <he>\n", n->name().c_str());
            goto error;
          }
      }
      break;
      
    case BL:
      {
        if (n->type() != LB)
          {
            fprintf(stderr, "WARNING: <bl> must enclose <lb>, not <%s>\n",
                    n->name().c_str());
            goto error;
          }
      }
      break;

    case LB:
      {
        if (n->block())
          {
            fprintf(stderr, "WARNING: text block tag <%s> not allowed in <lb>\n", n->name().c_str());
            goto error;
          }
      }
      break;


    case NL:
      {
        if (n->type() != LN)
          {
            fprintf(stderr, "WARNING: <nl> must enclose <ln>, not <%s>\n",
                    n->name().c_str());
            goto error;
          }
      }
      break;


    case LN:
      {
        if (n->block())
          {
            fprintf(stderr, "WARNING: text block tag <%s> not allowed in <ln>\n", n->name().c_str());
            goto error;
          }
      }
      break;
    
    case PD:
      {
        if (n->block())
          {
            fprintf(stderr, "WARNING: text block tag <%s> not allowed in <%s>\n", n->name().c_str(), name_.c_str());
            goto error;
          }
      }
      break;

    case QQ:
      {
        if (n->block())
          {
            fprintf(stderr, "WARNING: text block tag <%s> not allowed in <qq>\n", n->name().c_str());
            goto error;
          }
      }
      break;

    case CODE:
      {
        if (n->type() != TEXT)
          {
            fprintf(stderr, "WARNING: Markup is not permitted inside <%s>\n",
                    name_.c_str());
            goto error;
          }
      }
      break;

    case SIDEBAR:
      {
      }
      break;

    case FIGURE:
      {
        switch (n->type())
          {
          case IMAGEFILE:
          case CAPTION:
          case LABEL:
            break;
          default:
            fprintf(stderr, "WARNING: <%s> can't be inside <%s>\n",
                    n->name().c_str(), name_.c_str());
            goto error;
            break;
          }
      }
      break;

    case IMAGEFILE:
      {
        if (n->type() != TEXT)
          {
            fprintf(stderr, "WARNING: Markup is not permitted inside <imagefile>\n");
            goto error;
          }
      }
      break;

    case TABLE:
      {
        switch (n->type())
          {
          case TABLEHEAD:
          case TABLEBODY:
          case TABLECELL:
          case LABEL:
          case CAPTION:
            break;
          default:
            fprintf(stderr, "WARNING: <%s> can't be inside <%s>\n",
                    n->name().c_str(), name_.c_str());
            goto error;
            break;
          }
      }
      break;

    case CAPTION:
      {
        if (n->block())
          {
            fprintf(stderr, "WARNING: Block is not permitted inside <%s>\n",
                    name_.c_str());
            goto error;
          }
      }
      break;

    case TABLEHEAD:
      {
        if (n->type() != TABLECELL)
          {
            fprintf(stderr, "WARNING: tag <%s> not allowed in <%s> (<tablecell> only)\n", 
                    n->name().c_str(), name_.c_str());
            goto error;
          }
      }
      break;

    case TABLEBODY:
      {
        if (n->type() != TABLECELL)
          {
            fprintf(stderr, "WARNING: tag <%s> not allowed in <%s> (<tablecell> only)\n", 
                    n->name().c_str(), name_.c_str());
            goto error;
          }
      }
      break;

    case TABLECELL:
      {
        if (n->block())
          {
            fprintf(stderr, "WARNING: text block tag <%s> not allowed in <%s>\n", 
                    n->name().c_str(), name_.c_str());
            goto error;
          }
      }
      break;

    case FIXME:
      {
      }
      break;

    case FUNCLIST:
      {
        switch (n->type())
          {
          case FUNCHEADER:
          case FUNCSYN:
          case CAPTION:
          case LABEL:
            break;
          default:
            fprintf(stderr, "WARNING: <%s> can't be inside <%s>\n",
                    n->name().c_str(), name_.c_str());
            goto error;
            break;
          }
      }
      break;

    case FUNCHEADER:
      {
        if (n->type() != TEXT)
          {
            fprintf(stderr, "WARNING: Markup is not permitted inside <%s>\n",
                    name_.c_str());
            goto error;
          }
      }
      break;

    case FUNCSYN:
      {
        switch (n->type())
          {
          case FUNCNAME:
          case RETVAL:
          case ARG:
            break;
          default:
            fprintf(stderr, "WARNING: <%s> can't be inside <%s>\n",
                    n->name().c_str(), name_.c_str());
            goto error;
            break;
          }
      }
      break;

    case FUNCNAME:
      {
        if (n->type() != TEXT)
          {
            fprintf(stderr, "WARNING: Markup is not permitted inside <%s>\n",
                    name_.c_str());
            goto error;
          }
      }
      break;

    case RETVAL:
      {
        if (n->type() != TEXT)
          {
            fprintf(stderr, "WARNING: Markup is not permitted inside <%s>\n",
                    name_.c_str());
            goto error;
          }
      }
      break;

    case ARG:
      {
      }
      break;


    case ARGTYPE:
      {
        if (n->type() != TEXT)
          {
            fprintf(stderr, "WARNING: Markup is not permitted inside <%s>\n",
                    name_.c_str());
            goto error;
          }
      }
      break;

    case ARGNAME:
      {
        if (n->type() != TEXT)
          {
            fprintf(stderr, "WARNING: Markup is not permitted inside <%s>\n",
                    name_.c_str());
            goto error;
          }
      }
      break;

    case MACLIST:
      {
        switch (n->type())
          {
          case MACHEADER:
          case MACSYN:
          case CAPTION:
          case LABEL:
            break;
          default:
            fprintf(stderr, "WARNING: <%s> can't be inside <%s>\n",
                    n->name().c_str(), name_.c_str());
            goto error;
            break;
          }
      }
      break;

    case MACHEADER:
      {
        if (n->type() != TEXT)
          {
            fprintf(stderr, "WARNING: Markup is not permitted inside <%s>\n",
                    name_.c_str());
            goto error;
          }
      }
      break;

    case MACSYN:
      {
        switch (n->type())
          {
          case ARGNAME:
          case MACNAME:
          case VOID:
            break;
          default:
            fprintf(stderr, "WARNING: <%s> can't be inside <%s>\n",
                    n->name().c_str(), name_.c_str());
            goto error;
            break;
          }
      }
      break;

    case MACNAME:
      {
        if (n->type() != TEXT)
          {
            fprintf(stderr, "WARNING: Markup is not permitted inside <%s>\n",
                    name_.c_str());
            goto error;
          }
      }
      break;

    case LABEL:
    case CHAPREF:
    case SECREF:
    case FIGREF:
    case TABREF:
    case SIDEREF:
    case MLREF:
    case FLREF:
    case INCLUDE:
    case WIDGET:
    case C:
    case FILE:
    case FUNCTION:
    case HEADER:
    case MACRO:
    case MFOUR:
    case MODULE:
    case SIGNAL:
    case TERM:
    case TT:
    case EM:
    case URL:
      {
        if (n->type() != TEXT)
          {
            fprintf(stderr, "WARNING: Further markup <%s> is not permitted inside <%s>\n",
                    n->name().c_str(), name_.c_str());
            goto error;
          }
      }
      break;

    case VOID:
    case CXX:
    case GLIB:
    case GDK:
    case GTK:
    case GNOME:
    case IMLIB:
    case LINUX:
    case ORBIT:
    case UNIX:
      {
        fprintf(stderr, 
                "WARNING: Can't add tag <%s> inside <%s>,"
                "which must be empty.\n", 
                n->name().c_str(),
                name_.c_str());
        goto error;
      }
      break;

    case TEXT:
      {
        fprintf(stderr, "WARNING: Internal error, adding child to TEXT node?\n");
        goto error;
      }
      break;

    default: 
      {
        fprintf(stderr, "WARNING: internal error, do not know whether tag <%s> can go in <%s>\n",
                n->name().c_str(), name_.c_str());
        goto error;
      }
      break;

    }

  switch (n->type())
    {
    case BOOK:
      {
        fprintf(stderr, "WARNING: tag <%s> must not be contained in any other tag\n",
                n->name().c_str());
        goto error;
      }
      break;

    case TITLE:
    case AUTHOR:
    case COPYRIGHT:
    case PART:
    case P:
    case HA:
    case HB:
    case HC:
    case HD:
    case HE:
    case PD:
    case QQ:
    case SIDEBAR:
    case FIGURE:
    case TABLE:
      {
        if (type_ != BOOK)
          {
            fprintf(stderr, "WARNING: tag <%s> must be below the <book> tag\n",
                    n->name().c_str());
            goto error;
          }
      }
      break;

    case LB:
      {
        if (type_ != BL)
          {
            fprintf(stderr, "WARNING: <lb> must be inside <bl>\n");
            goto error;
          }
      }
      break;

    case LN:
      {
        if (type_ != NL)
          {
            fprintf(stderr, "WARNING: <ln> must be inside <nl>\n");
            goto error;
          }
      }
      break;

    case NL:
    case BL:
    case CODE:
      {
        if (!block())
          {
            fprintf(stderr, "WARNING: tag <%s> may not occur inside non-block <%s>\n", 
                    n->name().c_str(), name_.c_str());
            goto error;
          }
      }
      break;

      // BEGIN unfinished FIXME
    case CAPTION:
    case TABLEHEAD:
    case TABLEBODY:
    case TABLECELL:
    case FIXME:
    case FUNCLIST:
    case FUNCHEADER:
    case FUNCSYN:
    case FUNCNAME:
    case RETVAL:
    case ARG:
    case ARGTYPE:
    case ARGNAME:
    case VOID:
    case MACLIST:
    case MACHEADER:
    case MACSYN:
    case MACNAME:
    case LABEL:
    case CHAPREF:
    case SECREF:
    case FIGREF:
    case TABREF:
    case SIDEREF:
    case MLREF:
    case FLREF:
    case INCLUDE:
    case IMAGEFILE:
    case WIDGET:
    case C:
    case FILE:
    case FUNCTION:
    case HEADER:
    case MACRO:
    case MFOUR:
    case MODULE:
    case SIGNAL:
    case TERM:
    case TT:
    case EM:
    case URL:
    case CXX:
    case GLIB:
    case GDK:
    case GTK:
    case GNOME:
    case IMLIB:
    case LINUX:
    case ORBIT:
    case UNIX:
      break; // FIXME all these should have error checks...

    case TEXT:
      break;

    case COMMENT:
      break;
      
    default:
      fprintf(stderr, "WARNING: Internal error, don't know if <%s> can go in <%s>\n",
              n->name().c_str(), name_.c_str());
      break;
    }

  // OK! If we made it through that mess, add the child.
  children_.push_back(n);
  n->set_parent(this);
  return;

 error:
  fprintf(stderr, "WARNING:   (Adding node %u to %u)\n", n->id(), id());
  return;
}


void
Node::spew(::FILE* f, int depth)
{
  char spaces[256];
  memset(spaces, ' ', 256);
  
  if (depth > 80) return;

  spaces[depth*2] = '\0';

  if (f == NULL)
    {
      f = fopen("book.spew", "w");
      
      if (f == NULL)
        {
          fprintf(stderr, "Couldn't open spew file\n");
          return;
        }
    }

  fprintf(f, 
          "%6u %sNode <%s>  p: %d c: %d s: %d ss: %d sss: %d f: %d t: %d\n",
          id(),
          spaces,
          name_.c_str(),
          counter_.part(),
          counter_.chapter(),
          counter_.section(),
          counter_.subsection(),
          counter_.subsubsection(),
          counter_.figure(),
          counter_.table());

  vector<Node*>::iterator i = children_.begin();
  while (i != children_.end())
    {
      (*i)->spew(f, depth + 1);
      ++i;
    }
}

// This is crap but works for now
static string 
get_content(Node* n)
{
  const vector<Node*> & children = n->children();
  
  vector<Node*>::const_iterator i = children.begin();

  string content;

  while (i != children.end())
    {
      content += (*i)->text();

      ++i;
    }
  
  strip_leading_trailing(content);

  return content;
}

string 
funcsyn_to_func(Node* n, bool format)
{
  string fname;
  string retval;
  vector<string> args;

  vector<Node*>::const_iterator i = n->children().begin();
  while (i != n->children().end())
    {
      if ((*i)->type() == Node::FUNCNAME)
        {
          fname = get_content(*i);
        }
      else if ((*i)->type() == Node::ARG)
        {
          string argname;
          string argtype;
          vector<Node*>::const_iterator j = (*i)->children().begin();
          while (j != (*i)->children().end())
            {          
              if ((*j)->type() == Node::ARGTYPE)
                {
                  argtype = get_content(*j);
                }
              else if ((*j)->type() == Node::ARGNAME)
                {
                  argname = get_content(*j);
                }

              ++j;
            }
          string all = argtype;
          if (!all.empty()) all += " "; // for varargs, no type
          all += argname;
          args.push_back(all);
        }
      else if ((*i)->type() == Node::RETVAL)
        {
          retval = get_content(*i);
        }

      ++i;
    }   

  strip_leading_trailing(retval);
  
  string func = retval;

  if (format)
    func += "\n";
  else 
    func += " ";

  strip_leading_trailing(fname);

  func += fname + "(";
  
  bool firstarg = true;

  if (!args.empty())
    {
      unsigned int indent = fname.size()+1;
      
      vector<string>::iterator ai;
      ai = args.begin();
      while (ai != args.end())
        {
          if (format && !firstarg)
            {
              unsigned int q = 0;
              while (q < indent)
                {
                  func += ' ';
                  ++q;
                }
            }

          func += *ai;     

          ++ai;

          if (ai != args.end())
            {
              func += ",";
              if (format) func += "\n";
              else        func += " ";
            }
          else 
            func += ")";


          firstarg = false;
        }
    }
  else 
    {
      func += ")";
    }

  return func;
}

static void
funcsyn_to_tablerow(Node* n)
{
  n->set_type(Node::TABLEBODY);
  n->set_name("tablebody");

  Node* cell = new Node;
  cell->set_type(Node::TABLECELL);
  cell->set_name("tablecell");

  Node* function = new Node;
  function->set_type(Node::FUNCTION);
  function->set_name("function");
  
  Node* text = new Node;
  text->set_type(Node::TEXT);
  text->set_name("TEXT");

  cell->add(function);
  function->add(text);

  string func = funcsyn_to_func(n, false);

  text->set_text(func);

  n->clear_children();

  n->add(cell);
}

static void
funclist_to_table(Node* n)
{
  vector<Node*>::const_iterator i = n->children().begin();
  while (i != n->children().end())
    {
      if ((*i)->type() == Node::FUNCSYN)
        {
          funcsyn_to_tablerow(*i);
        }
      else if ((*i)->type() == Node::FUNCHEADER)
        {
          (*i)->set_type(Node::TABLEHEAD);
          (*i)->set_name("tablehead");
        }

      ++i;
    } 

  n->set_type(Node::TABLE);
  n->set_name("table");
}

string 
macsyn_to_mac(Node* n)
{
  string mac;

  bool opened = false;
  bool noargs = false;
  
  vector<Node*>::const_iterator i = n->children().begin();
  while (i != n->children().end())
    {
      if ((*i)->type() == Node::MACNAME)
        {
          mac += get_content(*i);
        }
      else if ((*i)->type() == Node::ARGNAME)
        {
          if (!opened)
            {
              opened = true;
              mac += "(";
            }
          mac += get_content(*i);
          mac += ", ";
        }
      else if ((*i)->type() == Node::VOID)
        {
          noargs = true;
        }
      
      ++i;
    } 

  if (opened)
    {
      if (mac.size() >  1)
        {
          if (mac[mac.size()-2] == ',')
            mac[mac.size()-2] = ')';
          else 
            mac += ')';
        }
    }
  else if (noargs)
    {
      mac += "()";
    }

  return mac;
}

static void
macsyn_to_tablerow(Node* n)
{
  n->set_type(Node::TABLEBODY);
  n->set_name("tablebody");

  Node* cell = new Node;
  cell->set_type(Node::TABLECELL);
  cell->set_name("tablecell");

  Node* macro = new Node;
  macro->set_type(Node::MACRO);
  macro->set_name("macro");
  
  Node* text = new Node;
  text->set_type(Node::TEXT);
  text->set_name("TEXT");

  cell->add(macro);
  macro->add(text);

  string mac = macsyn_to_mac(n);

  text->set_text(mac);

  n->clear_children();

  n->add(cell);
}

static void
maclist_to_table(Node* n)
{
  vector<Node*>::const_iterator i = n->children().begin();
  while (i != n->children().end())
    {
      if ((*i)->type() == Node::MACSYN)
        {
          macsyn_to_tablerow(*i);
        }
      else if ((*i)->type() == Node::MACHEADER)
        {
          (*i)->set_type(Node::TABLEHEAD);
          (*i)->set_name("tablehead");
        }

      ++i;
    } 

  n->set_type(Node::TABLE);
  n->set_name("table");
}

void
postprocess_tree(Node* n, Counter & c, bool tableize)
{
  // Perform transformations of the tree
  
  if (tableize)
    {
      if (n->type() == Node::FUNCLIST)
        {
          funclist_to_table(n);
        }
      
      if (n->type() == Node::MACLIST)
        {
      maclist_to_table(n);
        }
      
      if (n->type() == Node::MLREF || n->type() == Node::FLREF)
        {
          n->set_type(Node::TABREF);
          n->set_name("tabref");
        }
    }

  // Update counters
  switch (n->type())
    {
    case Node::PART:
      c.inc_part();
      break;
    case Node::HA:
      c.inc_chapter();
      break;
      //    case Node::HB:
    case Node::HC:
      c.inc_section();
      break;
    case Node::HD:
      c.inc_subsection();
      break;
    case Node::HE:
      c.inc_subsubsection();
      break;
    case Node::FIGURE:
      c.inc_figure();
      break;
    case Node::TABLE:
      c.inc_table();
      break;
    case Node::MACLIST:
      c.inc_maclist();
      break;
    case Node::FUNCLIST:
      c.inc_funclist();
      break;
    default:
      break;
    }

  n->counter() = c;

  if (n->type() == Node::LABEL)
    {
      string label = get_content(n);
      strip_leading_trailing(label);
      get_refs().insert(label, n->counter());
    }

  vector<Node*>::const_iterator i = n->children().begin();
  while (i != n->children().end())
    {
      postprocess_tree(*i, c, tableize);
      ++i;
    }
}

const Counter* 
get_ref(Node* n)
{
  string label = get_content(n);
  strip_leading_trailing(label);
  const Counter* c = get_refs().lookup(label);
  return c;
}

string
get_ref_string(Node* n)
{
  string label = get_content(n);
  strip_leading_trailing(label);
  return label;
}
