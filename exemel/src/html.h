// -*- C++ -*-
// Copyright 1999 Havoc Pennington. All Rights Reserved.


#ifndef HTML_H
#define HTML_H

#include "output.h"

class HTML : public Output {
public:
  virtual void output(FILE* f,
                      Node* n);
};

#endif
