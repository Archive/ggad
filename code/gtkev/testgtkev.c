
#include "gtkev.h"

#include <gtk/gtk.h>

static gint delete_event_cb(GtkWidget* w, GdkEventAny* e, gpointer data);

int 
main(int argc, char* argv[])
{
  GtkWidget* window;
  GtkWidget* ev;

  gtk_init(&argc, &argv);  

  window = gtk_window_new(GTK_WINDOW_TOPLEVEL);

  ev = gtk_ev_new();

  gtk_container_add(GTK_CONTAINER(window), ev);
  gtk_window_set_title(GTK_WINDOW(window), "GtkEv Test");

  gtk_signal_connect(GTK_OBJECT(window),
                     "delete_event",
                     GTK_SIGNAL_FUNC(delete_event_cb),
                     NULL);

  gtk_widget_show_all(window);

  gtk_main();

  return 0;
}

static gint 
delete_event_cb(GtkWidget* window, GdkEventAny* e, gpointer data)
{
  gtk_main_quit();
  return FALSE;
}









