/*
 * Example widget 'GtkEv'
 *
 * A widget that displays events, similar to "xev"
 *
 * Developed by Havoc Pennington <hp@pobox.com>
 *
 */

/*** gtkevh */

#ifndef INC_GTK_EV_H
#define INC_GTK_EV_H

#include <gtk/gtkwidget.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define GTK_EV(obj)          GTK_CHECK_CAST (obj, gtk_ev_get_type (), GtkEv)
#define GTK_EV_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, gtk_ev_get_type (), GtkEvClass)
#define GTK_IS_EV(obj)       GTK_CHECK_TYPE (obj, gtk_ev_get_type ())

/*** gtkevstructs */
typedef struct _GtkEv       GtkEv;
typedef struct _GtkEvClass  GtkEvClass;

struct _GtkEv
{
  GtkWidget widget;
  
  GdkWindow*     event_window;

  GdkRectangle   event_window_rect;

  GdkRectangle   description_rect;

  GList*         buffer;
  GList*         buffer_end;
  gint           buffer_size;
};

struct _GtkEvClass
{
  GtkWidgetClass parent_class;


};

/* gtkevstructs ***/

guint           gtk_ev_get_type           (void);
GtkWidget*      gtk_ev_new                (void);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GTK_EV_H__ */

/* gtkevh ***/
