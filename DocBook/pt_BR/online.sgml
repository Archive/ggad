<!--<!doctype appendix PUBLIC "-//Conectiva SA//DTD livros V1.0//EN">-->

<appendix id="online">
  <title>Recursos on-line</title>
  <para>Este cap�tulo documenta alguns recursos on-line �teis para
  programadores do Gnome. 

  <indexterm>
      <primary>Gnome</primary>
      <secondary>recursos on-line</secondary>
  </indexterm>

</para> 
  
  <sect1 id="sec-online-libs">
    <title>Obtendo e compilando as bibliotecas</title>
    <para>Como as instru��es de compila��o exatas podem mudar entre os
    releases, este livro n�o tentar� document�-las precisamente. Voc�
    deve obter o a vers�o est�vel mais recente tanto do GTK+ como do
    Gnome em <ulink url="http://www.gtk.org/"
    type="http">http://www.gtk.org/</ulink> e em <ulink
    url="http://www.gnome.org"
    type="http">http://www.gnome.org</ulink>, respectivamente, e
    seguir as instru��es que v�m com a distribui��o. 

    <indexterm>
	<primary>GTK+</primary>
        <secondary>bibliotecas, Web site</secondary>
    </indexterm>

    <indexterm>     
        <primary>Gnome (GNU Network Object Model
        Environment)</primary> 
        <secondary>inicializando</secondary>
        <tertiary>Web site</tertiary>
    </indexterm>

    <indexterm>   
        <primary>s�tios da web</primary>
        <secondary>bibliotecas, GTK+/Gnome</secondary>
    </indexterm>

    <indexterm>
        <primary>bibliotecas</primary>
        <secondary>Gtk+, s�tio da Web</secondary>	
    </indexterm>

    <indexterm>
        <primary>bibliotecas</primary>
        <secondary>Gnome</secondary>
        <tertiary>S�tio da Web</tertiary>
    </indexterm>

    <indexterm>
	<primary>bibliotecas</primary>
        <secondary>obtendo e compilando as</secondary>
    </indexterm>

	
</para> 
    <para>� uma boa id�ia voc� mesmo compilar as bibliotecas do GTK+
    do Gnome, por v�rias raz�es:</para>  
    <itemizedlist>
      <listitem>
	<para>Deve-se ter o c�digo fonte por perto, para aprender e
	consult�-lo quando estiver sem a documenta��o.</para> 
      </listitem>
      <listitem>
	<para>Deve-se compilar com depura��o ativada, desse modo as
	pr�-condi��es e assertivas ser�o desencadeadas se voc� tentar
	utilizar a biblioteca incorretamente. A maioria dos bin�rios
	embutidos desativa essas verifica��es por quest�es de
	efici�ncia.</para> 
      </listitem>
      <listitem>
	<para>Voc� provavelmente desejar� ter as bibliotecas abertas,
	assim poder� ver a os nomes de fun��o etc. em seu
	depurador.</para> 
      </listitem>
    </itemizedlist>
  </sect1>

  <sect1 id="sec-online-web">
    <title>Sites da Web</title>
    <para>O GTK+ e o Gnome t�m home pages; <ulink
    url="http://www.gtk.org/" type="http">http://www.gtk.org/</ulink> 
    e <ulink url="http://www.gnome.org"
    type="http">http://www.gnome.org</ulink>, respectivamente. 

    <indexterm>
	<primary>Gnome (GNU Network Object Model
        Environment)</primary> 
        <secondary>Web Site</secondary>
    </indexterm>

    <indexterm>
        <primary>GTK+</primary>
        <secondary>Web site</secondary>
    </indexterm>

    <indexterm>   
        <primary>s�tios da web</primary>
        <secondary>GTK+/Gnome</secondary>
    </indexterm>

    <indexterm>
	<primary>web</primary>
        <secondary>s�tios da</secondary>
    </indexterm>

</para> 
    <para>No site do GTK+, tome uma nota especial dessas
    p�ginas:</para>  
    <itemizedlist>
      <listitem>
	<para><ulink url="http://www.gtk.org/rdp/"
	type="http">http://www.gtk.org/rdp/</ulink> � o GTK+ Reference
	Documentation Project; veja aqui materiais de refer�ncia sobre
	o GTK+ API.</para> 
      </listitem>
      <listitem>
	<para><ulink url="http://www.gtk.org/faq/"
	type="http">http://www.gtk.org/faq/</ulink> tem a GTK+ FAQ;
	fazer essas perguntas na lista de endere�os n�o �
	recomend�vel.  O Ap�ndice C deste livro supplementa o GTK+ FAQ
	oficial com ainda mais perguntas; verifique-o tamb�m.</para> 
      </listitem>
    </itemizedlist>
    <para>No site do Gnome, d� uma olhada nestas p�ginas:</para> 
    <itemizedlist>
      <listitem>
	<para><ulink url="http://www.gnome.org/lxr/"
	type="http">http://www.gnome.org/lxr/</ulink> cont�m c�pias de
	hipertexto naveg�veis e pesquis�veis de todos os c�digos do
	lado servidor do GTK+/Gnome CVS.Isso inclui o GTK+ e o Gnome,
	assim como muitos aplicativos.</para> 
      </listitem>
      <listitem>
	<para><ulink url="http://bugs.gnome.org/"
	type="http">http://bugs.gnome.org/</ulink> permite a voc�
	naveguar por artigos de bug do GTK+ e Gnome e submeter
	novos. Se encontrar um bug, por favor submeta-o aqui para que
	os mantenedores possam monitor�-lo.</para> 
      </listitem>
      <listitem>
        <para><ulink
        url="http://developer.gnome.org"type="http">http://developer.gnome.org</ulink> 
        Ainda n�o estava ativo na �poca da publica��o original deste
        livro, mas estar� breve. Ele ir� conter fontes abrangentes
        para desenvolvedores em Gnome.</para> 
      </listitem>
    </itemizedlist>
  </sect1>

  <sect1 id="sec-online-mail">
    <title>Listas de discuss�o</title>
    <para>Eis algumas das listas de discuss�o que abrangem t�picos
    de desenvolvimento do GTK+ e do Gnome: 

    <indexterm>
	<primary>GTK+</primary>
        <secondary>listas de discuss�o</secondary>
    </indexterm>

    <indexterm>
        <primary>Gnome (GNU Network Object Model
        Environment)</primary> 
        <secondary>listas de discuss�o</secondary>
    </indexterm> 

    <indexterm>
	<primary>listas de discuss�o, GTK+/Gnome
	development</primary> 
    </indexterm>
			
    <indexterm>
	<primary>discuss�o</primary>
        <secondary>listas de</secondary>
    </indexterm>

</para> 
    <itemizedlist>
      <listitem>
	<para><application role="tt">gtk-list@redhat.com</application>
	� apropriado para quest�es sobre a utiliza��o de GTK+, leitura
	de c�digo-fonte do GTK+, discuss�o de bugs do GTK+ etc.</para> 
      </listitem>
      <listitem>
	<para><application
	role="tt">gnome-list@gnome.org</application> tem tr�fego muito
	alto; ele � a lista de endere�os do Gnome geral, para a
	discuss�o de todas as coisas relacionadas com o Gnome.</para> 
      </listitem>
      <listitem>
	<para><application
	role="tt">gnome-devel-list@gnome.org</application> � a lista
	certa para quest�es sobre as bibliotecas do Gnome e como
	escrever programas com elas. � apropriada tamb�m para discutir
	o desenvolvimento das bibliotecas em si ou submeter patches
	que modificam as bibliotecas.</para> 
      </listitem>
      <listitem>
	<para><application
	role="tt">gtk-app-devel-list@redhat.com</application> � uma
	alternativa � <application role="tt">gtk-list</application>
	mais focalizada no desenvolvimento de aplicativo. Muitas
	coisas apropriadas para essa lista tamb�m s�o apropridas para
	a <application role="tt">gtk-list</application>, mas
	<application role="tt">gtk-app-devel-list</application> tem
	menos tr�fego se voc� estiver preocupado com isso.</para> 
      </listitem>
      <listitem>
	<para><application
	role="tt">gtk-devel-list@redhat.com</application> � para
	discuss�o do desenvolvimento das bibliotecas glib, GDK e
	GTK+. Ela <emphasis>n�o</emphasis> � para a discuss�o sobre
	<emphasis>utiliza��o</emphasis>das bibliotecas para fins de
	desenvolvimento de aplicativos.</para> 
      </listitem>
      <listitem>
	<para><application
	role="tt">gnome-announce-list@gnome.org</application> traz
	an�ncios relacionados ao Gnome e aos aplicativos Gnome. Seu
	tr�fego � relativamente baixo.</para> 
      </listitem>
    </itemizedlist>
    <para>Para inscrever-se para em dessas listas de discuss�o,
    acrescente <application role="tt">-request</application> ao nome
    da lista e envie uma mensagem com <application
    role="tt">subscribe my-address@wherever.net</application> no campo
    de assunto. Por exemplo, voc� poderia se increver em <application
    role="tt">gtk-list@redhat.com</application> enviando uma mensagem
    para <application
    role="tt">gtk-list-request@redhat.com</application> com
    <application role="tt">subscribe hp@pobox.com</application> na
    linha de assunto.</para>  
    <para>� recomend�vel que voc� obtenha alguma ou todas as quest�es
    sobre o desenvolvimento de GTK+ e Gnome de uma das listas de
    mala-direta ou de um IRC. � inconveniente solicitar isso
    diretamente aos desenvolvedores, a menos que voc� tenha raz�o
    para acreditar que eles tenham um conhecimento especial sobre
    alguma se��o de c�digo particular. De qualquer maneira, voc�
    obter� quase certamente uma reposta melhor e mais r�pida a partir
    de uma lista. 

    <indexterm>     
        <primary>Gnome (GNU Network Object Model
        Environment)</primary> 
        <secondary>listas de discuss�o</secondary>
    </indexterm>
			
</para> 
  </sect1>

  <sect1 id="sec-online-irc">
    <title>Internet Relay Chat</title>
    <para>Os dois principais canais de IRC relacionados com
    desenvolvimento do GTK+ e do Gnome est�o ambos em <application
    role="tt">irc.gimp.org</application>; participe de <application
    role="tt">#gimp</application> ou <application
    role="tt">#gnome</application> para fazer perguntas. Voc� tamb�m
    pode encontrar desenvolvedores de biblioteca e outros
    participantes da comunidade GTK+/Gnome. 

    <indexterm>
        <primary>GTK+</primary>
        <secondary>canais IRC (Internet Relay Chat)</secondary>
    </indexterm>

    <indexterm>
        <primary>IRC (Internet Relay Chat) canais, desenvolvimento do
        GTK+ e do Gnome</primary> 
    </indexterm>

    <indexterm>
	<primary>internet relay chat</primary>
    </indexterm>

    <indexterm>
        <primary>Gnome (GNU Network Object Model Environment)</primary>
        <secondary>IRC (internet relay chat) canais</secondary>
    </indexterm>
  
</para> 
  </sect1>

  <sect1 id="sec-online-book">
    <title>Este livro</title>
    <para>Este livro est� dispon�vel on-line em <ulink
    url="http://developer.gnome.org/doc/GGAD"
    type="http">http://developer.gnome.org/doc/GGAD</ulink> 

    <indexterm>
	<primary>on-line</primary>
        <secondary>este livro, </secondary>
     </indexterm>

</para> 
  </sect1>
</appendix>



